import numpy as np 
import librosa 
import torch 
import os
import sys
import glob
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from audio_project_ew.dataset import SegmentGenerator, AudioGenerator
from audio_project_ew.workers import VADWorker, DenoiserWorker
from audio_project_ew.torch_utils import step_loader, get_default_device, DeviceDataLoaderVAD, DeviceDataLoaderNoise
import json
import torch
import logging, logging.config
from audio_project_ew.logger import LOGGING_CONFIG

logging.config.dictConfig(LOGGING_CONFIG)
logger = logging.getLogger('develop')

logging.getLogger('develop').setLevel(logging.NOTSET)

def fit_vad():
    config = json.loads(open('../config.json').read())
    config_test = json.loads(open('../config.json').read())
    config_test['data']['phase'] = 'TEST'
    logging.getLogger('develop').setLevel(logging.NOTSET)
    train_gen = SegmentGenerator(config['data'])
    test_gen = SegmentGenerator(config_test['data'])
    device = get_default_device()#torch.device('cpu')#
    sampler = torch.utils.data.sampler.WeightedRandomSampler(train_gen.tensor_weights_per_class, len(train_gen.tensor_weights_per_class))                     
    train_loader = DeviceDataLoaderVAD(torch.utils.data.DataLoader(train_gen, 
                                                                batch_size=config['model']['batch_size'], 
                                                                shuffle = False,
                                                                sampler=sampler,
                                                                num_workers=config['model']['num_workers']),device=device)
    test_loader = DeviceDataLoaderVAD(torch.utils.data.DataLoader(test_gen, 
                                                                batch_size=config['model']['batch_size'], 
                                                                shuffle = True, 
                                                                num_workers=config['model']['num_workers']),device=device)
    test_loader_step = DeviceDataLoaderVAD(torch.utils.data.DataLoader(test_gen, 
                                                                batch_size=config['model']['batch_size'], 
                                                                shuffle = False, 
                                                                num_workers=config['model']['num_workers']),device=device)
    test_loader_step = step_loader(test_loader_step)
    
    learner = VADWorker(config['model'])
    if config['model']['resume']:
        learner.load_ckpt('latest')
    logger.info(f'Model: {learner.model}')
    
    max_epoch_acc = 0
    for epoch in range(learner.clock.epoch, config['model']['epochs']):
        learner.train_batch(train_loader, test_loader_step, epoch)
        epoch_acc = learner.evaluate_batch(test_loader)
        torch.cuda.empty_cache()
        
        if epoch_acc > max_epoch_acc:
            learner.save_ckpt('best_acc')
            max_epoch_acc = epoch_acc

        learner.update_learning_rate()
        learner.save_ckpt('latest')

def fit_denoiser():
    config = json.loads(open('../config.json').read())
    config_test = json.loads(open('../config.json').read())
    config_test['data']['phase'] = 'TEST'
    train_gen = AudioGenerator(config['data'])
    test_gen = AudioGenerator(config_test['data'])
    device = get_default_device()#torch.device('cpu')#
    learner = DenoiserWorker(config['model'])
    sampler = torch.utils.data.sampler.WeightedRandomSampler(train_gen.tensor_weights_per_class, len(train_gen.tensor_weights_per_class))                     
    train_loader = DeviceDataLoaderNoise(torch.utils.data.DataLoader(train_gen, 
                                                                batch_size=config['model']['batch_size'], 
                                                                shuffle = True, 
                                                                #sampler = sampler, 
                                                                num_workers=config['model']['num_workers']),device=device)
    test_loader = DeviceDataLoaderNoise(torch.utils.data.DataLoader(test_gen, 
                                                                batch_size=config['model']['batch_size'], 
                                                                shuffle = True, 
                                                                num_workers=config['model']['num_workers']),device=device)
    test_loader_step = DeviceDataLoaderNoise(torch.utils.data.DataLoader(test_gen, 
                                                                batch_size=config['model']['batch_size'], 
                                                                shuffle = True, 
                                                                num_workers=config['model']['num_workers']),device=device)
    test_loader_step = step_loader(test_loader_step)
    
    
    if config['model']['resume']:
        learner.load_ckpt('latest')
    logger.info(f'Model: {learner.model}')
    
    max_epoch_loss = 1e5
    for epoch in range(learner.clock.epoch, config['model']['epochs']):
        learner.train_batch(train_loader, test_loader_step ,epoch)
        epoch_loss = learner.evaluate_batch(test_loader)
        torch.cuda.empty_cache()
        
        if epoch_loss < max_epoch_loss:
            learner.save_ckpt('best_loss')
            max_epoch_loss = epoch_loss

        learner.update_learning_rate()
        learner.save_ckpt('latest')

if __name__ == '__main__':
    fit_denoiser()
    #fit_vad()
