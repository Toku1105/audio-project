import argparse
from pathlib import Path

path = Path(__file__).parent.absolute()

def parseArgs():
    parser = argparse.ArgumentParser(description='VAD and DENOISER TESTING')
    parser.add_argument('--infer_mode','-im', choices=['vad','denoiser'], required=True, help='VAD or DENOISER mode')
    parser.add_argument('--vad_model_path', type=str, default=f'{path}/models/VAD/best_acc.pth')
    parser.add_argument('--denoiser_model_path', type=str, default=f'{path}/models/DENOISER/best_loss.pth')
    parser.add_argument('--input_audios_path', type=str, default=f'{path}/data/input', help='only path where audios are')
    parser.add_argument('--input_audio_names','-ins', nargs='+', default=['test.wav'], help='could be multiple audio separated with space')
    parser.add_argument('--output_vad_path', type=str, default=f'{path}/data/output/vad', help='path to save VAD results')
    parser.add_argument('--output_denoiser_path', type=str, default=f'{path}/data/output/denoiser', help='path to save DENOISER results')
    parser.add_argument('--mode','-m', type=str, default='gpu', choices = ['cpu', 'gpu'], help='inference cpu or gpu')
    parser.add_argument('--batch_size', '-bs', type=int, default=16, help='to split single audio in batch of small portions, returns full audios')
    parser.add_argument('--max_secs','-s', type=int, default=5, help='to split single audio in small portions (MEMORY FAULT) return full audio')
    
    args = parser.parse_args()
    return args

def parseRequestArgs():
    parser = argparse.ArgumentParser(description='VAD and DENOISER TESTING')
    parser.add_argument('--infer_mode','-im', choices=['vad','denoiser'], required=True, help='VAD or DENOISER mode')
    parser.add_argument('--model_ip_port', type=str)
    parser.add_argument('--input_audios_path', type=str, default=f'{path}/data/input', help='only path where audios are')
    parser.add_argument('--input_audio_names','-ins', nargs='+', default=['test.wav'], help='could be multiple audio separated with space')
    parser.add_argument('--output_vad_path', type=str, default=f'{path}/data/output/vad', help='path to save VAD results')
    parser.add_argument('--output_denoiser_path', type=str, default=f'{path}/data/output/denoiser', help='path to save DENOISER results')
    parser.add_argument('--batch_size', '-bs', type=int, default=16, help='to split single audio in batch of small portions, returns full audios')
    parser.add_argument('--max_secs','-s', type=int, default=5, help='to split single audio in small portions (MEMORY FAULT) return full audio')
    
    args = parser.parse_args()
    return args