#docker run --rm --shm-size=1g         --ulimit memlock=-1         --ulimit stack=67108864         -p8080:8080         -p8081:8081         -p8082:8082         -p7070:7070         -p7071:7071         --mount type=bind,source=/home/oscar/data/audio-project/model-server/model-store,target=/tmp/models --name vad_serve torchserve:1.0 torchserve --model-store=/tmp/models --models vad=vad_model.mar denoiser=denoiser_model.mar
import sys
import glob
import inspect
import os 
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

from audio_project_ew.torch_utils import reconstruct_from_spec
import parser
import librosa
import numpy as np 
import soundfile as sf
import json
import torch
import requests
import gzip


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.bool_):
            return super(CustomJSONEncoder, self).encode(bool(obj))

        return super(CustomJSONEncoder, self).default(obj)

def silence_noise(audio:np.ndarray,
               segments:list,
               gap:float=0.0,)->np.ndarray:
        ''' audio cutter, cut and join segments or audio given a dictionary conatining a bool isspeech
            Args:
                audio(np.ndarray): audio only 1 channel
                segments(list(dict)): list of dictionaries including flag isspeech, start and stop of segment
                sr(int): sampling rate
                gap(float): seconds to add its calculated as sr*gap
            Returns:
                audio(np.ndarray): segmented array
        '''
        tmp_list = []
        
        for segment in segments:
            if segment["is_speech"]:
                tmp_list.append(audio[segment["start"] : segment["stop"]])
            else:
                tmp_list.append([0.0]*int(segment["stop"] - segment["start"]))
        audio = np.concatenate(tmp_list) if tmp_list else []
        
        return audio

def cutter(audio:np.ndarray,
            segments:list,
            sr:int=16000,
            gap:float=0.0,)->np.ndarray:
        ''' audio cutter, cut and join segments or audio given a dictionary conatining a bool isspeech
            Args:
                audio(np.ndarray): audio only 1 channel
                segments(list(dict)): list of dictionaries including flag isspeech, start and stop of segment
                sr(int): sampling rate
                gap(float): seconds to add its calculated as sr*gap
            Returns:
                audio(np.ndarray): segmented array
        '''
               
        tmp_list = []
        
        for segment in segments:
            if segment["is_speech"]:
                tmp_list.append(audio[segment["start"] : segment["stop"]])
                tmp_list.append([0.0]*int(sr*gap))
        audio = np.concatenate(tmp_list) if tmp_list else []
        return audio

def merge_segments(segments:list, min_gap:float = 0. )->list:
    ''' Merge continuous segments for cleaner list, ig min_gap set this function will look for
        segments shorter than min_gap(seconds) and will set is_speech with the value from previous segment.
            Args:
                segments(list(dict)): list of dictionaries including flag isspeech, start and stop of segment
                min_gap(float): seconds to consider to keep the segment with its value
            Returns:
                segments(List(dict)): list of dictionaries
    '''
    merged = []
    for seg in segments: 
        if not merged or merged[-1]['is_speech'] != seg['is_speech']:
            merged.append(seg) 
        else: 
            merged[-1]['stop'] = seg['stop']

    for i in range(len(merged)):
        if min_gap > 0 and merged[i]['stop'] - merged[i]['start'] < min_gap * 16000:
            merged[i]['is_speech'] = merged[i-1]['is_speech'] if i>0 else merged[i+1]['is_speech']

    return merged
    
def save_audio(data:np.ndarray,
                path:str,
                sr:int=16000)->str:
        ''' Save file audio given path and sampling rate
            Args:
                data(np.ndarray): audio 
                path(str): path to save
                sr(int): sampling rate
            Returns:
                tuple with path and name of audio
        '''
        if not path or os.path.splitext(path)[-1] != '.wav':
            raise ValueError('Provide path to save audio')
        
        sf.write(path, data, samplerate=sr)
        print(f'Audio file saved {path}')
        
        return os.path.split(path)

class Test(object):

    def __init__(self,
        infer_mode='vad',
        model_ip_port=None,
        input_audios_path=None,
        input_audio_names=None,
        output_vad_path=None,
        output_denoiser_path=None,
        batch_size=16,
        max_secs= 5,
        ): 

        self.batch_size = batch_size
        self.size = max_secs * 16000 # seconds*sampling_rate
        self.infer_mode = infer_mode
        self.model_ip_port = model_ip_port 
        self.input_audios_paths = [os.path.join(input_audios_path,x) for x in input_audio_names]
        self.output_vad_path = output_vad_path
        self.output_denoiser_path = output_denoiser_path
    
    def load_audios(self):
        """ Generator for audio due currently we dont have handler to use batches with different lengths"""
        for audio_path in self.input_audios_paths:
            audio, sr = librosa.load(audio_path,sr=16000,mono=True)
            audio = np.divide(audio,np.max(np.abs(audio)), out=np.zeros_like(audio, dtype=float),where=np.max(np.abs(audio))!=0)
            yield audio, os.path.splitext(os.path.split(audio_path)[-1])[0]

    def save_vad(self, 
            audio_wo_noise:np.ndarray, 
            segments:list,
            name:str,
            save_audio_:bool = True,
            save_segments:bool=True):

        if save_audio_:
            save_audio(data=audio_wo_noise,
                path=os.path.join(self.output_vad_path, name+'.wav'))

        if save_segments:
            with open(os.path.join(self.output_vad_path,name+'.txt'),'w') as f:
                json.dump(segments,f,cls=CustomJSONEncoder)
                print(f'Segments saved in {os.path.join(self.output_vad_path,name+".txt")}')
                
    def save_denoiser(self, 
                        clean:np.ndarray,
                        recovered:np.ndarray,
                        noise:np.ndarray,
                        name:str,
                        save_audio_:bool = True,
                        save_noise:bool = True,):

        if save_audio_:
            save_audio(data=clean,
                path=os.path.join(self.output_denoiser_path, name+'_denoiser_vad.wav'))
            save_audio(data=recovered,
                path=os.path.join(self.output_denoiser_path, name+'_denoiser.wav'))
        
        if save_noise:
            save_audio(data=noise,
                path=os.path.join(self.output_denoiser_path, name+'_noise.wav'))

    def process_denoiser(self, 
                        audio_spectrogram:torch.Tensor,
                        noise_spectrogram:torch.Tensor
                        ):

        recovered = audio_spectrogram.numpy()
        noise = noise_spectrogram.numpy()
        recovered = np.divide(recovered,np.max(np.abs(recovered)), out=np.zeros_like(recovered, dtype=float),where=np.max(np.abs(recovered))!=0)
        noise = np.divide(noise,np.max(np.abs(noise)), out=np.zeros_like(noise, dtype=float),where=np.max(np.abs(noise))!=0)

        return recovered, noise

    def get_batches(self,audio):
        remaining = audio.shape[-1]%self.size
        b = audio[:remaining].reshape((1,-1))
        audio = audio[remaining:].reshape((-1,self.size))
        if b.shape[-1]:
            print(f'Processing initial {b.shape}')
            if b.shape[-1] < 16000:
                b = np.pad(b[-1],(16000-b.shape[-1],0),'constant').reshape((1,-1))
            yield b
        for i in range(0, audio.shape[0], self.batch_size):
            batch = audio[i:i+self.batch_size]
            yield batch

    def parse_request(self,request,tag:str=''):
        if request.status_code == 200: 
            output = json.loads(gzip.decompress(request.content).decode('utf-8'))
            return output
        else:
            raise RuntimeError(f'No response from server {tag} {request.status_code}')

    def get_denoiser_data(self,vad_data):

        req = requests.post(f"http://{self.model_ip_port}/predictions/denoiser", json=vad_data)
        output = self.parse_request(req,'denoiser')

        recovered, noise = self.process_denoiser(torch.FloatTensor(output['recovered_audio_spec']),
                        torch.FloatTensor(output['noise_pred_spec']))

        return recovered, noise

    def get_vad_data(self,batch):
        req = requests.post(f"http://{self.model_ip_port}/predictions/vad", json=batch.tolist())
        output = self.parse_request(req,'vad')
        return output 

    def update_segments(self, tmp_segments, last_index=0):
        vad_segments = []
        for j, segments in enumerate(tmp_segments):
            for segment in segments:
                start = segment['start']+last_index
                stop = segment['stop']+last_index
                segment.update({'start':start,'stop':stop,'is_speech':True if segment['is_speech']=='true' else False})
            vad_segments.extend(segments)
            last_index = vad_segments[-1]['stop']
        return vad_segments

    def process_audio(self, audio):
        recovered_audio = np.array([])
        noise_audio = np.array([])
        vad_segments = []

        for i, batch in enumerate(self.get_batches(audio),1):
            print(f'Processing batch {i}, {batch.shape}')

            vad_data = self.get_vad_data(batch)
            last_index = vad_segments[-1]['stop'] if len(vad_segments)>0 else 0
            tmp_segments = self.update_segments(vad_data['segments'], last_index)
            vad_segments.extend(tmp_segments)

            del vad_data['segments']

            if self.infer_mode == 'denoiser':
                tmp_recovered_audio, tmp_noise_audio = self.get_denoiser_data(vad_data)
                recovered_audio = np.append(recovered_audio,tmp_recovered_audio)
                noise_audio = np.append(noise_audio,tmp_noise_audio)

        return vad_segments, recovered_audio, noise_audio


    def run(self):

        audios = self.load_audios()

        for audio, name in audios:
            print(audio.shape)
            vad_segments, recovered_audio, noise_audio = self.process_audio(audio)
            vad_segments = merge_segments(vad_segments, 0.1)
            vad_segments = merge_segments(vad_segments, 0)
            vad_audio = cutter(audio, vad_segments)
            if self.infer_mode == 'denoiser':
                clean = np.asarray(silence_noise(recovered_audio,vad_segments))   
                self.save_denoiser(clean,recovered_audio, noise_audio, name)  
            print(len(vad_audio))
            self.save_vad(vad_audio, vad_segments, name)


if __name__ == '__main__':
    args = vars(parser.parseRequestArgs())
    test = Test(**args)
    test.run()