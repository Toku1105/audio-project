# UPDATE DEPLOYED CLUSTER 
## Copy and replace all folders from raid/files except mongo
## Copy and replace all content of KUBERNETES
## Update previous flaskapp deployment with 
kubectl apply -f 8-flaskapp.yaml
## Load Image of new app with ASR, LID, GID 
kind load image-archive kaldi_flask.tar
## Apply Deployment of new APP
kubectl apply -f 7-speechapp.yaml

# DONE!

# FROM SCRATCH 
## Install kubectl and kind
chmod +x ./kind
chmod +x ./kubectl
## We need kubernetes installed however we can virtualize it with kind
docker load -i kind.v1.21.1.tar
## Modify cluster.yaml with required hostPath (raid/files), 
## create folder mongodb, copy: config, model-store,speech_api, speech_app
## verify ports or change them hostPort. NOT TO CHANGE CONTAINER PORTS
kind create cluster --image kindest/node:v1.21.1 --config kuberentes/cluster.yaml
# LOAD IMAGES
kind load image-archive ingress-nginx.tar 
kind load image-archive kube-webhook-certgen.tar 
kind load image-archive mongo.tar 
kind load image-archive mongo-express.tar (OPTIONAL)
kind load image-archive torchserve-cpu.tar
kind load image-archive flask_app.tar
kind load image-archive kaldi_flask.tar
# APPLY YAML FILES 
cd kubernetes
kubectl apply -f 0-storageClass.yaml 
kubectl apply -f 1-mongo-secret.yaml
kubectl apply -f 2-mongo.yaml
kubectl apply -f 3-mongo-configmap.yaml 
kubectl apply -f 4-mongo-express.yaml (OPTIONAL)
kubectl apply -f 5-torchserver-configmap.yaml
## Modify torchserve.yaml for cpu limit and memory limit
kubectl apply -f 6-torchserve.yaml
kubectl apply -f 7-speechapp.yaml
kubectl apply -f 8-flaskapp.yaml
openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout tls.key -out tls.crt -subj "/CN=audioapp.ai" -days 365 kubectl create secret tls tls-cert --cert=cert.pem --key=key.pem
kubectl create secret tls app-secret-tls --cert=tls.crt --key=tls.key 
kubectl apply -f 9-nginx-operator.yaml 
kubectl wait --namespace ingress-nginx \
--for=condition=ready pod \
--selector=app.kubernetes.io/component=controller \
--timeout=90s
kubectl apply -f 10-ingressapp.yaml 

# Other Commands
### kubectl get commands
# List all your containers
kubectl get pods 

# If you want to see the log (prints) of an app(container) 
# copy the name displayed by get pods and then 
kubectl logs ${POD_NAME}

# If a container is giving ERROR or is not RUNNING when you apply a kubernetes YAML
# get the POD_NAME and then
kubectl describe pod ${POD_NAME}
# Or you could get the logs of that POD if the error is not about the image itself but it is about functionality of the container (file missing of folder missing or permissions)

# If deployment (apply command) went wrong or you want to delete a POD for some reason like delete all logs, or something
kubectl delete -f ${YAML_FILE}
kubectl apply -f ${YAML_FILE}
# However if you apply on top of a running It will update it which is recommended 

# ANOTHER USEFUL COMMANDS
kubectl get pod --watch
kubectl get pod -o wide
kubectl get service
kubectl get secret
kubectl get all | grep mongodb