# INSTRUCTIONS

Clone Repository  
**NOTE: INCLUDE ALL SUBMODULES**

### Build Docker Container
Build from root (audio-project folder) of repo since it will be included in docker build context. 

Attention to .dockerignore, **Ignore .dockerignore is expensive and potentially dangerous** (included in root of repo)


```
docker build -t {IMAGE_NAME} -f docker/Dockerfile . 
```
### Run Container
EXPOSE PORT 8000 -> Python API 

EXPOSE PORT 8501 -> tensorflow_serving (internal and optional)

```
docker run -t --rm --name {CONTAINER_NAME} -p 8000:8000 {IMAGE_NAME} 
```

## REQUEST PROCEDURE 
### ENDPOINTS 
**/files ->** return name of last file updated (FIFO max 10 elements)

**/files/{filename} ->** return file, to download

**/files/upload ->** endpoint to POST file and return: -> **redirection to /predict/{filename}** -> return  json with predictions 

**/predict/{filename} ->** optional endpoint to return prediction of audio from file in server(refer to /files endpoint)


**NOTE:** include "name" in params, since it will be the name asigned to that file, however if you forget this parameter a random name will be assigned 


## EXAMPLE 

```
import requests
#filename = 'filename'
headers = {
    "Content-Type": "audio/wav",
}
params = {"uploadType": "media", "name": f"{filename}.wav"}

url = 'http://localhost:8000/files/upload/'

with open(f'./{filename}.wav', 'rb') as file:
	file =file.read()
	r =requests.post(url, params=params, headers=headers, data=file)

print(r.text)
``` 
# RETURN EXAMPLE

> {"0":{"ARABIC":0.0943361446,
> "DARI":7.83651956e-07,
> "ENGLISH":0.886830509,
> "FARSI":0.000118472453,
> "HINDI":0.00733117945,
> "PASHTO":0.00978220347,
> "URDU":0.00160070125}}


