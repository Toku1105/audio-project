//webkitURL is deprecated but nevertheless
URL = window.URL || window.webkitURL;

var gumStream;                      //stream from getUserMedia()
var rec;                            //Recorder.js object
var input;                          //MediaStreamAudioSourceNode we'll be recording

// shim for AudioContext when it's not avb. 
var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext //audio context to help us record

var recordButton = document.getElementById("recordButton");
var stopButton = document.getElementById("stopButton");
var pauseButton = document.getElementById("pauseButton");
let ts;
var tmpTime = 0, remaining = 0;
//canvas 
var audioInput = null,
    realAudioInput = null,
    inputPoint = null,
    audioRecorder = null;
var rafID = null;
var rAF = null;
var analyserContext = null;
var canvasWidth, canvasHeight;
var recIndex = 0;


//add events to those 2 buttons

recordButton.addEventListener("click", startRecording);
stopButton.addEventListener("click", stopRecording);
pauseButton.addEventListener("click", pauseRecording);

function createSegments(elements){
    var segments = []
    for (let i = 0; i < elements.length; i++){
        if (elements[i].is_speech=="true"){
            segments.push({
                start: elements[i].start/16000,
                end: elements[i].stop/16000,
                loop: false,
                color: 'hsla(136, 72%, 29%, 0.4)',
                drag:false,
                resize:false,
            });
        };

    };
    return segments
};
function load_audio(elem) {
    // console.log(elem.dataset.audio)
    x = elem.dataset.audio
    x = x.replace(/'/g, '"')
    x = JSON.parse(x);
    
    if (x.hasOwnProperty('name')){
        orig = x.name.substr(0, x.name.lastIndexOf('.')) || x.name
        wave_orig = document.getElementById(`waveform${orig}`);
    };
    segments = []
    if (x.hasOwnProperty('vad_segments')){
        segments = createSegments(x.vad_segments)
    };

    if (x.hasOwnProperty('deepvad')){
        vad = x.deepvad.substr(0, x.deepvad.lastIndexOf('.')) || x.deepvad
        wave_vad = document.getElementById(`waveform${vad}`);
    };

    if (x.hasOwnProperty('denoised_vad')){
        denoiser = x.denoised_vad.substr(0, x.denoised_vad.lastIndexOf('.')) || x.denoised_vad
        wave_denoiser = document.getElementById(`waveform${denoiser}`);
    };
    
    if ((x.hasOwnProperty('name'))&&!(wave_orig.firstChild)){
        var wavesurfer_orig = WaveSurfer.create({
            container: `#waveform${orig}`,
            waveColor: 'hsla(0, 100%, 25%, 1.0)',
            //progressColor: 'hsla(248, 53%, 58%, 0.9)',
            cursorColor: 'hsla(0, 0%, 50%, 1.0)',
            // This parameter makes the waveform look like SoundCloud's player
            //barWidth: 1,
            plugins: [
                WaveSurfer.regions.create({
                    regions: segments,
                    enableDragSelection:false
                }),
                WaveSurfer.timeline.create({
                    container: `#wave-timeline${orig}`,
                    timeInterval: 0.1
                }),
                
            ]
        });
        wavesurfer_orig.load('/static/tmp/uploads/'+orig+'.wav');
        play_orig = document.getElementById(`play${orig}`);
        play_orig.addEventListener('click', function() {

            wavesurfer_orig.playPause();

        });
    
    };
    // if ((x.hasOwnProperty('deepvad'))&&!(wave_vad.firstChild)){
    //     var wavesurfer_vad = WaveSurfer.create({
    //         container: `#waveform${vad}`,
    //         waveColor: 'grey',
    //         progressColor: 'hsla(200, 100%, 30%, 0.5)',
    //         cursorColor: '#fff',
    //         // This parameter makes the waveform look like SoundCloud's player
    //         barWidth: 1
    //         });
    //     wavesurfer_vad.load('/static/tmp/uploads/'+vad+'.wav');
    //     play_vad = document.getElementById(`play${vad}`);
    //     play_vad.addEventListener('click', function() {

    //         wavesurfer_vad.playPause();
    
    //         if (this.textContent === 'Play') {
    //             this.textContent = "Pause";
    //         } else {
    //             this.textContent = "Play";
    //         }
    //     });
    // };

    if ((x.hasOwnProperty('denoised_vad'))&&!(wave_denoiser.firstChild)){

        var wavesurfer_denoiser = WaveSurfer.create({
            container: `#waveform${denoiser}`,
            waveColor: 'hsla(0, 100%, 25%, 1.0)',
            //progressColor: 'hsla(248, 53%, 58%, 0.9)',
            cursorColor: 'hsla(0, 0%, 50%, 1.0)',
            // This parameter makes the waveform look like SoundCloud's player
            //barWidth: 1,
            plugins: [
                WaveSurfer.timeline.create({
                    container: `#wave-timeline${denoiser}`,
                    timeInterval: 0.1
                }),
                
            ]
            });
        wavesurfer_denoiser.load('/static/tmp/uploads/'+denoiser+'.wav');
        play_denoiser = document.getElementById(`play${denoiser}`);
        play_denoiser.addEventListener('click', function() {

            wavesurfer_denoiser.playPause();
        });
    
    };

}


function cancelAnalyserUpdates() {
    window.cancelAnimationFrame(rafID);
    rafID = null;
}

function updateAnalysers(time) {
    if (!analyserContext) {
        var canvas = document.getElementById("analyser");
        canvasWidth = canvas.width;
        canvasHeight = canvas.height;
        analyserContext = canvas.getContext('2d');
    }

    // analyzer draw code here
    {
        var SPACING = 3;
        var BAR_WIDTH = 1;
        var numBars = Math.round(canvasWidth / SPACING);
        var freqByteData = new Uint8Array(analyserNode.frequencyBinCount);

        analyserNode.getByteFrequencyData(freqByteData);

        analyserContext.clearRect(0, 0, canvasWidth, canvasHeight);
        analyserContext.fillStyle = '#F6D565';
        analyserContext.lineCap = 'round';
        var multiplier = analyserNode.frequencyBinCount / numBars;

        // Draw rectangle for each frequency bin.
        for (var i = 0; i < numBars; ++i) {
            var magnitude = 0;
            var offset = Math.floor(i * multiplier);
            // gotta sum/average the block, or we miss narrow-bandwidth spikes
            for (var j = 0; j < multiplier; j++)
                magnitude += freqByteData[offset + j];
            magnitude = magnitude / multiplier;
            var magnitude2 = freqByteData[i * multiplier];
            analyserContext.fillStyle = "hsl( " + Math.round((i * 360) / numBars) + ", 100%, 50%)";
            analyserContext.fillRect(i * SPACING, canvasHeight, BAR_WIDTH, -magnitude);
        }
    }

    rafID = window.requestAnimationFrame(updateAnalysers);
}


function startRecording() {
    //console.log("recordButton clicked");

    /*
        Simple constraints object, for more advanced audio features see
        https://addpipe.com/blog/audio-constraints-getusermedia/
    */

    var constraints = { audio: true, video:false }

    /*
        Disable the record button until we get a success or fail from getUserMedia() 
    */

    recordButton.disabled = true;
    stopButton.disabled = false;
    pauseButton.disabled = false;

    /*
        We're using the standard promise based getUserMedia() 
        https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
    */

    navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
        //console.log("getUserMedia() success, stream created, initializing Recorder.js ...");


        audioContext = new AudioContext();
        if (!navigator.cancelAnimationFrame)
            navigator.cancelAnimationFrame = navigator.webkitCancelAnimationFrame || navigator.mozCancelAnimationFrame;
        if (!navigator.requestAnimationFrame)
            navigator.requestAnimationFrame = navigator.webkitRequestAnimationFrame || navigator.mozRequestAnimationFrame;
        

        /*  assign to gumStream for later use  */
        gumStream = stream;

        /* use the stream */
        input = audioContext.createMediaStreamSource(stream);
        rAF = requestAnimationFrame(outputTimestamps);
        
        analyserNode = audioContext.createAnalyser();
        analyserNode.fftSize = 2048;
        input.connect(analyserNode);
        //input.connect(timeNode);
        /* 
            Create the Recorder object and configure to record mono sound (1 channel)
            Recording 2 channels  will double the file size
        */
        rec = new Recorder(input,{numChannels:1})

        //start the recording process
        
        rec.record()
        updateAnalysers();
        //console.log("Recording started");

    }).catch(function(err) {
        //enable the record button if getUserMedia() fails
        recordButton.disabled = false;
        stopButton.disabled = true;
        pauseButton.disabled = true
    });
}

function pauseRecording(){
    //console.log("pauseButton clicked rec.recording=",rec.recording );
    if (rec.recording){
        //pause
        rec.stop();
        tmpTime = ts.contextTime;
        cancelAnimationFrame(rAF);
        pauseButton.innerHTML="Resume";
    }else{
        //resume
        rec.record()
        rAF = requestAnimationFrame(outputTimestamps);
        ts = audioContext.getOutputTimestamp()
        remaining += ts.contextTime-tmpTime
        //console.log('timing: '+tmpTime+' lalal: ' + ts.contextTime+' rm: ' +remaining)
        
        pauseButton.innerHTML="Pause";

    }
}
function outputTimestamps() {
  ts = audioContext.getOutputTimestamp()
  document.getElementById("formats").innerHTML="Format: 1 channel pcm @ "+audioContext.sampleRate/1000+"kHz"+' | time: ' + (ts.contextTime-remaining).toFixed(2);


  rAF = requestAnimationFrame(outputTimestamps);
}
function stopRecording() {
    //console.log("stopButton clicked");

    //disable the stop button, enable the record too allow for new recordings
    stopButton.disabled = true;
    recordButton.disabled = false;
    pauseButton.disabled = true;

    //reset button just in case the recording is stopped while paused
    pauseButton.innerHTML="Pause";

    //tell the recorder to stop the recording
    rec.stop();
    tmpTime = 0
    remaining = 0
    cancelAnimationFrame(rAF);

    //stop microphone access
    gumStream.getAudioTracks()[0].stop();

    //create the wav blob and pass it on to createDownloadLink
    rec.exportWAV(createDownloadLink);
}

function createDownloadLink(blob) {

    var url = URL.createObjectURL(blob);
    var au = document.createElement('audio');
    var li = document.createElement('li');
    var link = document.createElement('a');

    //name of .wav file to use during upload and download (without extendion)
    var filename = new Date().toISOString();

    //add controls to the <audio> element
    au.controls = true;
    au.src = url;

    //save to disk link
    link.href = url;
    link.download = filename+".wav"; //download forces the browser to donwload the file using the  filename
    link.innerHTML = "Save to disk";

    //add the new audio element to li
    li.appendChild(au);

    //add the filename to the li
    li.appendChild(document.createTextNode(filename+".wav "))

    //add the save to disk link to li
    li.appendChild(link);

    //upload link
    var submit = document.createElement('a');
    var upload_anim = document.createElement('span');
    var arrow = document.createElement('span');
    var ok = document.createElement('span');
    
    upload_anim.className = 'uploadd'
    arrow.className = "glyphicon glyphicon-arrow-up arrow";
    arrow.setAttribute("aria-hidden",'true');
    ok.className = "glyphicon glyphicon-ok ok";
    ok.setAttribute("aria-hidden",'true');
    submit.href="javascript:void(0)";
    submit.innerHTML = "Submit";
    var xhr=new XMLHttpRequest();
    // xhr.addEventListener( 'load', function( event ) {
    // alert( 'Yeah! Data sent and response loaded.' );} );
    // xhr.addEventListener(' error', function( event ) {
    // alert( 'Oops! Something went wrong.' );} );
    
    var fd=new FormData();
    fd.append("audio_data",blob, filename);
    submit.addEventListener("click", function myfunc(event){
        xhr.onload=function(e) {
        upload_anim.classList.toggle('uploading');
        arrow.classList.toggle('arrow-out');
        if(arrow.classList.contains('arrow-out')){
          ok.classList.add('bounce');
        }else{
          ok.classList.remove('bounce');
        }
        if(this.readyState === 4) {
                  submit.innerHTML = "";
                  submit.href="";
                  submit.removeEventListener("click",myfunc);
                  var results = document.getElementById("results")
                  results.style.display = "block";
                  
                  
              }
          };
    xhr.open("POST","https://"+window.location.hostname +"/audio");
    xhr.send(fd);

    });

    li.appendChild(document.createTextNode (" "));//add a space in between
    li.appendChild(submit);//add the upload link to li
    li.appendChild(upload_anim);
    li.appendChild(arrow);
    li.appendChild(ok);

    //add the li element to the ol
    recordingsList.appendChild(li);
}