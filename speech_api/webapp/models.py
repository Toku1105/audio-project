import uuid
from flask import session, flash
from flask_login import UserMixin
from .extensions import mongo, login_manager
from werkzeug.security import generate_password_hash, check_password_hash
import secrets

db = mongo.db
records = db.records

class User(UserMixin):

    def __init__(self, username, password, _id=None, api_key = None):

        self.username = username
        self.password = password
        self._id = uuid.uuid4().hex if _id is None else _id
        self.api_key = api_key

    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return self._id

    @classmethod
    def get_by_username(cls, username):
        data = records.find_one({"username": username})
        if data is not None:
            return cls(**data)

    @classmethod
    def get_by_id(cls, _id):
        data = records.find_one({"_id": _id})
        if data is not None:
            return cls(**data)
    
    @classmethod
    def get_by_apikey(cls, key):
        data = records.find_one({"api_key": key})
        if data is not None:
            return cls(**data)

    @staticmethod
    def login_valid(username, password):
        verify_user = User.get_by_username(username)
        if verify_user and check_password_hash(verify_user.password, password):
            session['username'] = username
            return True
        return False

    @classmethod
    def register(cls, username, password):
        user = cls.get_by_username(username)
        if user is None:
            new_user = cls(username, password)
            new_user.save_to_mongo()
            session['username'] = username
            return True
        else:
            return False

    @classmethod 
    def api_unregister(cls, username):
        user = cls.get_by_username(username)
        if user is not None: 
            user.api_key = None
            user.api_to_mongo()
            return True
        return False

    @classmethod 
    def api_register(cls, username):
        user = cls.get_by_username(username)
        if user is not None: 
            user.api_key = secrets.token_urlsafe(15) if user.api_key is None else user.api_key
            user.api_to_mongo()
            return True
        return False

    def api_to_mongo(self):
        id_ = records.update_one({'_id':self._id},
            {"$set" :{'api_key':self.api_key}},upsert=False).upserted_id

    def json(self):
        return {
            "username": self.username,
            "_id": self._id,
            "password": generate_password_hash(self.password),
            "api_key":self.api_key
        }

    def save_to_mongo(self):
        records.insert(self.json())