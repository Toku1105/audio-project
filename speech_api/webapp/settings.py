import os 

MONGO_URI = os.environ.get('MONGO_URI')
SECRET_KEY = os.environ.get('SECRET_KEY')
TORCHSERVE_URI = os.environ.get('TORCHSERVE_URI')
SPEECHAPP_URI = os.environ.get('SPEECHAPP_URI')
