import librosa
import numpy as np
from matplotlib import pyplot as plt
import os


def plot_waveform(waveform, sample_rate, title="Waveform", xlim=None, ylim=None, save_path=None, segments=None):
    #waveform = waveform.numpy()
    if len(waveform.shape) < 2:
        waveform = waveform.reshape(1,-1)
    num_channels, num_frames = waveform.shape
    time_axis = np.arange(0, num_frames) / sample_rate

    figure, axes = plt.subplots(num_channels, 1)
    if num_channels == 1:
        axes = [axes]
    
    for c in range(num_channels):
        if segments:
            for s in segments:
                color = 'blue' if s['is_speech']=='true' else 'red'
                axes[c].plot(time_axis[s['start']:s['stop']], waveform[c][s['start']:s['stop']], linewidth=1, color=color)
        else:
            axes[c].plot(time_axis, waveform[c], linewidth=1)
        axes[c].grid(True)
        if num_channels > 1:
            axes[c].set_ylabel(f'Channel {c+1}')
        if xlim:
            axes[c].set_xlim(xlim)
        if ylim:
            axes[c].set_ylim(ylim)
    figure.suptitle(title)
    if save_path:
        plt.savefig(save_path)
        return os.path.split(save_path)
    plt.show(block=False)
