from flask import Flask 
from flask_bootstrap import Bootstrap
from flask_dropzone import Dropzone
from werkzeug.middleware.proxy_fix import ProxyFix
import os
import webapp.api_utils
from .extensions import mongo, login_manager, audios_
from flask_uploads import configure_uploads
from datetime import timedelta

def create_app(config_object='webapp.settings'):
	app = Flask(__name__)
	
	Bootstrap(app)
	dropzone = Dropzone(app)

	app.config['USER_APP_NAME'] = "Flask App"
	app.config['CSRF_ENABLED'] = True
	# Dropzone setting
	app.config['DROPZONE_UPLOAD_MULTIPLE'] = True
	app.config['DROPZONE_PARALLEL_UPLOADS'] = 6 
	app.config['DROPZONE_ALLOWED_FILE_CUSTOM'] = True
	app.config['DROPZONE_ALLOWED_FILE_TYPE'] = 'audio/*,.sph,.flac,.mp3'
	app.config['DROPZONE_REDIRECT_VIEW'] = 'main.home'
	app.config['DROPZONE_MAX_FILE_SIZE'] = 50
	app.config['DROPZONE_SERVE_LOCAL'] = True
	app.config['MAX_CONTENT_LENGTH'] = 50*1024*1024
	app.config['DROPZONE_MAX_FILES'] = 500
	app.config['DROPZONE_FILE_TOO_BIG'] = 'FILE TOO BIG'
	app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(days=10)
	# Uploads settings
	app.config['UPLOADED_AUDIOS_DEST'] = os.path.join(app.static_folder,'tmp/uploads')
	app.config.from_object(config_object)
	app.wsgi_app = ProxyFix(app.wsgi_app, x_proto=1)

	mongo.init_app(app)
	
	configure_uploads(app, audios_)

	login_manager.login_view = 'auth.login'
	login_manager.refresh_view = "auth.logout"
	login_manager.needs_refresh_message = ("To protect your account, please reauthenticate to access this page.")
	login_manager.needs_refresh_message_category = "info"
	login_manager.init_app(app)

	with app.app_context():
		from .auth import auth
		app.register_blueprint(auth)
		from .application import main
		app.register_blueprint(main)
		return app
