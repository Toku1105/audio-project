import os
import math
import struct
import librosa
import webrtcvad
import numpy as np
import soundfile as sf

import json
import requests
import gzip

import logging
logger = logging.getLogger('develop')

class Audio:
    
    def __init__(self, 
                 sr:int=16000,filepath:str=None):
        self.sr = sr
        self.filepath = filepath
        self.audio = self.read(filepath) if filepath else None 
        self.size = 2*16000
        self.batch_size = 4
        
    
    def read(self,
             filepath:str,
             mono:bool=False,
             channel:int=None,
             sr:int=None,
             upload=False
             )->list:
        ''' Read audio.
            Args:
                path: File path.
                mono: if False(default) read all channels
                channel: if mono=True return channel selected, otherwise return all channels 
                sr: sampling rate to read, default(self sr instance)
            Returns:
                audio(list(arrays)): return list of arrays each channel
        '''
        sr = sr if sr else self.sr
        self.filepath = filepath
        try:
            audio, sr = librosa.load(self.filepath,
                                     sr=sr,
                                     mono=mono)

            if channel is not None and isinstance(audio[channel], np.ndarray):
                try:
                    audio = np.expand_dims(audio[channel], axis=0)
                except:
                    logger.warning(f"Channel {channel} does not exist: using default 0")
                    audio = np.expand_dims(audio[0], axis=0)
            else:
                audio = np.expand_dims(audio, axis=0)

        except Exception as ex:
            logger.exception(f"Exception opening file with librosa. File {self.filepath}, exception: {ex}")
            return []

        tmp_audio = []
        for channel in audio:
            if upload:
                maxcutoff = channel.mean() + 4*channel.std()
                mincutoff = channel.mean() - 4*channel.std()
                a = np.clip(channel,mincutoff, maxcutoff)
                a = np.interp(a, (a.min(), a.max()), (-1, 1))
                a = np.divide(a,np.max(np.abs(a)), out=np.zeros_like(a, dtype=float),where=np.max(np.abs(a))!=0)
                remaining = a.shape[-1]%self.size
                if 0 < remaining < 16000:
                    a = np.pad(a,(16000-remaining,0),'constant')
            else:
                a = channel
            tmp_audio.append(a)
        self.audio = tmp_audio
        return self.audio
        

    def process_denoiser(self, 
                        audio_spectrogram:np.ndarray,
                        noise_spectrogram:np.ndarray
                        ):
        ''' Read audio.
            Args:
                audio_spectrogram:np.ndarray : Spectrogram of recovered audio
                noise_spectrogram:np.ndarray : Spectrogram of Noise profile
            Returns:
                audio(list(arrays)): return list of arrays each channel
        '''

        recovered = audio_spectrogram
        noise = noise_spectrogram
        recovered = np.divide(recovered,np.max(np.abs(recovered)), out=np.zeros_like(recovered, dtype=float),where=np.max(np.abs(recovered))!=0)
        noise = np.divide(noise,np.max(np.abs(noise)), out=np.zeros_like(noise, dtype=float),where=np.max(np.abs(noise))!=0)

        return recovered, noise


    def update_segments(self, tmp_segments, last_index=0):
        vad_segments = []
        for j, segments in enumerate(tmp_segments):
            for segment in segments:
                start = segment['start']+last_index
                stop = segment['stop']+last_index
                segment.update({'start':start,
                                'stop':stop,
                                })#'is_speech':True if segment['is_speech']=='true' else False})
            vad_segments.extend(segments)
            last_index = vad_segments[-1]['stop']
        return vad_segments

    def get_batches(self,audio):
        remaining = audio.shape[-1]%self.size
        b = audio[:remaining].reshape((1,-1))
        audio = audio[remaining:].reshape((-1,self.size))
        
        if b.shape[-1]:
            print(f'Processing initial {b.shape}')
            if b.shape[-1] < 16000:
                b = np.pad(b[-1],(16000-b.shape[-1],0),'constant').reshape((1,-1))
            yield b
        for i in range(0, audio.shape[0], self.batch_size):
            batch = audio[i:i+self.batch_size]
            yield batch

    def parse_request(self,request,tag:str=''):
        if request.status_code == 200: 
            output = json.loads(gzip.decompress(request.content).decode('utf-8'))
            return output
        else:
            raise RuntimeError(f'No response from server {tag} {req.status_code}')

    def get_vad_data(self,batch,model_uri):
        req = requests.post(model_uri, json=batch.tolist())
        output = self.parse_request(req,'vad')
        return output 

    def get_denoiser_data(self,vad_data,model_uri):

        req = requests.post(model_uri, json=vad_data)
        output = self.parse_request(req,'denoiser')

        recovered, noise = self.process_denoiser(np.asarray(output['recovered_audio_spec']),
                        np.asarray(output['noise_pred_spec']))

        return recovered, noise

    def process_audio(self, 
                    audio, 
                    model_uri, 
                    infer_mode='vad'):

        recovered_audio = np.array([])
        noise_audio = np.array([])
        vad_segments = []

        for i, batch in enumerate(self.get_batches(audio),1):
            print(f'Processing batch {i}, {batch.shape}')

            vad_data = self.get_vad_data(batch,model_uri+'vad')
            last_index = vad_segments[-1]['stop'] if len(vad_segments)>0 else 0
            tmp_segments = self.update_segments(vad_data['segments'], last_index)
            vad_segments.extend(tmp_segments)

            del vad_data['segments']

            if infer_mode == 'denoiser':
                tmp_recovered_audio, tmp_noise_audio = self.get_denoiser_data(vad_data, model_uri+'denoiser')
                recovered_audio = np.append(recovered_audio,tmp_recovered_audio)
                noise_audio = np.append(noise_audio,tmp_noise_audio)

        return vad_segments, recovered_audio, noise_audio

    def merge_segments(self,segments:list, min_gap:float = 0. )->list:
        ''' Merge continuous segments for cleaner list, ig min_gap set this function will look for
            segments shorter than min_gap(seconds) and will set is_speech with the value from previous segment.
                Args:
                    segments(list(dict)): list of dictionaries including flag isspeech, start and stop of segment
                    min_gap(float): seconds to consider to keep the segment with its value
                Returns:
                    segments(List(dict)): list of dictionaries
        '''
        merged = []
        for seg in segments: 
            if not merged or merged[-1]['is_speech'] != seg['is_speech']:
                merged.append(seg) 
            else: 
                merged[-1]['stop'] = seg['stop']

        for i in range(len(merged)):
            if min_gap > 0 and merged[i]['stop'] - merged[i]['start'] < min_gap * 16000:
                merged[i]['is_speech'] = merged[i-1]['is_speech'] if i>0 else merged[i+1]['is_speech']

        return merged

    def deep_vad_denoiser(self,
                 audio:np.ndarray,
                 model_uri = None,
                 infer_mode='vad',
                 )->list:

        ''' Our DeepVAD
            Args:
                audio(np.array): audio only 1 channel
            Returns:
                segments(list): return list of segments with boolean True if is speech otherwise False
        '''
        vad_segments, recovered_audio, noise_audio = self.process_audio(audio, model_uri, infer_mode)
        vad_segments = self.merge_segments(vad_segments, 0.1)
        vad_segments = self.merge_segments(vad_segments, 0)
        vad_audio = self.cutter(audio, vad_segments)
        clean = np.array([])
        if infer_mode == 'denoiser':
            clean = np.asarray(self.silence_noise(recovered_audio,vad_segments))   
        print(len(vad_audio))
        return vad_segments, vad_audio, clean, noise_audio

    
    def webrtc_vad(self,
            audio:np.ndarray,
            aggressiveness:int=3,
            window_duration:float=0.020,
            bytes_per_sample:int=2,
            sr:int=None,)-> list:
        ''' VAD webrtc
            Args:
                audio(np.array): audio only 1 channel
                aggressiveness(int): 0-3
                windows_duration(float): duration of windows ms 
                bytes_per_sample(int): 2 default int16, at this moment different value is not supported
                sr: sampling rate to read, default(self sr instance)
            Returns:
                segments(list): return list of segments with boolean True if is speech otherwise False
        '''
        sr = sr if sr else self.sr
        audio = (audio * 32767).astype(np.int16)
        vad = webrtcvad.Vad()
        vad.set_mode(aggressiveness)
        samples_per_window = int(window_duration * sr)
        segments = []
        audio_bytes = struct.pack("%dh" % len(audio), *audio)  # this only works for int16
        for start in np.arange(0, len(audio), samples_per_window):
            stop = min(start + samples_per_window, len(audio))
            samp = audio_bytes[start * bytes_per_sample : stop * bytes_per_sample]
            # complete bytearray if size is not divisible by 16bits*2*10
            size = 16 * bytes_per_sample * 10
            to_add = (size - len(samp) % (size)) % (size)
            correction = b"\x00" * to_add
            is_speech = vad.is_speech((samp + correction), sample_rate=sr)
            segments.append(dict(start=start, stop=stop, is_speech=is_speech))

        return segments
    
    def cutter(self,
               audio:np.ndarray,
               segments:list,
               sr:int=None,
               gap:float=0.0,)->np.ndarray:
        ''' audio cutter, cut and join segments or audio given a dictionary conatining a bool isspeech
            Args:
                audio(np.ndarray): audio only 1 channel
                segments(list(dict)): list of dictionaries including flag isspeech, start and stop of segment
                sr(int): sampling rate
                gap(float): seconds to add its calculated as sr*gap
            Returns:
                audio(np.ndarray): segmented array
        '''
               
        tmp_list = []
        sr = sr if sr else self.sr
        
        for segment in segments:
            if segment["is_speech"]=='true':
                tmp_list.append(audio[segment["start"] : segment["stop"]])
                tmp_list.append([0.0]*int(sr*gap))
        audio = np.concatenate(tmp_list) if tmp_list else []
        
        return audio

    
    def save_audio(self, 
                   data:np.ndarray=None,
                   path:str=None,
                   sr:int=None)->str:
        ''' Save file audio given path and sampling rate
            Args:
                data(np.ndarray): audio 
                path(str): path to save
                sr(int): sampling rate
            Returns:
                tuple with path and name of audio
        '''
        data = data if data is not None else self.audio[0]
        sr = sr if sr else self.sr
        path = path if path else self.filepath
        if path == self.filepath:
            logger.info(f'Updating Audio to same path: {self.filepath}')
        if not path or os.path.splitext(path)[-1] != '.wav':
            raise ValueError('Provide path to save audio')
        
        sf.write(path, data, samplerate=sr)
        logger.info(f'Audio file saved {path}')
        
        return os.path.split(path)

    def silence_noise(self,
               audio:np.ndarray,
               segments:list,
               gap:float=0.0,)->np.ndarray:
        ''' audio cutter, cut and join segments or audio given a dictionary conatining a bool isspeech
            Args:
                audio(np.ndarray): audio only 1 channel
                segments(list(dict)): list of dictionaries including flag isspeech, start and stop of segment
                sr(int): sampling rate
                gap(float): seconds to add its calculated as sr*gap
            Returns:
                audio(np.ndarray): segmented array
        '''
        tmp_list = []
        
        for segment in segments:
            if segment["is_speech"]=='true':
                tmp_list.append(audio[segment["start"] : segment["stop"]])
            else:
                tmp_list.append([0.0]*int(segment["stop"] - segment["start"]))
        audio = np.concatenate(tmp_list) if tmp_list else []
        
        return audio

    def mix_audios(self,
                   audio:np.ndarray,
                   noise:np.ndarray, 
                   SNR:float,
                   stochastic:bool=True,
                   mode:str='wrap',)->np.ndarray:
        ''' Mix two audios given SNR 
            Args:
                audio(np.ndarray): audio reference.
                noise(np.ndarray): noise to add
                SNR(float): SNR value to mix
                stochastic(bool): if audio and noise have different sizes, 
                                  they will be mixed by cutting or padding noise signal,
                                  to get same size of audio signal otherwise padding will
                                  be at then end of the array
            Returns:
                audio(list): list of np.ndarrays(audios)
        '''
        data = data if data is not None else self.audio[0]
        sr = sr if sr else self.sr
        path = path if path else self.filepath

        noise = cut_or_pad(noise, len(audio), stochastic, mode)
        # snr = math.exp(SNR/10)
        # scale = snr * np.linalg.norm(noise)/np.linalg.norm(audio)
        # noise = (scale*audio+noise)/2
        # return noise
        
        RMS_s = math.sqrt(np.mean(audio**2))
        RMS_n = math.sqrt(RMS_s**2/(pow(10,SNR/10)))
        RMS_n_current = math.sqrt(np.mean(noise**2))
        noise = noise*(RMS_n/RMS_n_current)
        audio = audio + noise
        audio = np.divide(audio,np.max(np.abs(audio)), out=np.zeros_like(audio, dtype=float),where=np.max(np.abs(audio))!=0)
        return audio

