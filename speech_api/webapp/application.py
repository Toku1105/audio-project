import os
import time
import math 
import numpy as np 
from collections import defaultdict
from .models import User
from flask import abort, Flask, render_template, redirect, request, url_for, flash, jsonify, Blueprint, current_app, Response
from flask_uploads import UploadSet, configure_uploads, AllExcept, SCRIPTS,EXECUTABLES
from flask_login import login_required, current_user, fresh_login_required, login_user
import webapp.api_utils as api_utils
from .extensions import mongo, audios_
from multiprocessing import Process

context={
    'now':int(time.time()),
    'strftime':time.strftime } 


main = Blueprint('main',__name__)

#patch_request_class(current_app)  # set maximum file size, default is 16MB
db = mongo.db
collection = db.audio
records = db.records
tmp_memory = []


@main.route('/', methods=['GET', 'POST'])
@main.route('/home', methods=['GET', 'POST'])
@login_required
def home():
    # Pagination
    idx = request.args.get('page',1, type=int)
    db_size = collection.count_documents({})
    pag = pagination(idx=idx, 
                     db_size=db_size)
    context.update(pag)
    return render_template(
        'pages/home.html',
        db_size=db_size,
        current=idx,
        **context,
    )

@main.route('/about')
def about():
    return render_template('pages/about.html', active='about')

@main.route('/save')
@login_required
def save():
    global tmp_memory
    tmp_memory = []
    flash('DATASET SAVED')
    return redirect(url_for('main.home'))

@main.route('/reset')
@login_required
def reset():
    reset_table(False)
    return redirect(url_for('main.home'))

@main.route('/drop')
@login_required
def drop():
    if current_user.username !='admin':
        abort(404)
    reset_table()
    return redirect(url_for('main.home'))


@main.route('/audio', methods=['GET','POST']) 
@login_required
def record():

    if 'audio' in request.url and request.method == 'POST':
        
        f = request.files['audio_data']
        filename,ext = api_utils.save_audio(f)
        p = api_utils.load_and_save(os.path.join(current_app.config['UPLOADED_AUDIOS_DEST'],filename+ext))
        tmp_files = {}
        tmp_files.update({'name':filename,
                          'url':os.path.join(os.path.split(request.url)[0],
                                            f'_uploads/audios/{filename}{ext}'),
                          'LANGUAGE_ID':{'UNKNOWN':0.0},
                          'ext':ext})
        t = time.process_time()
        #tmp_files.update(check_audio_for_minors(
        #                           os.path.join(app.config['UPLOADED_AUDIOS_DEST'],
        #                                  filename))[0])
        elapsed_time = time.process_time() - t
        flash('[INFO] INFERENCE TIME: ',str(elapsed_time))

        add_to_table(tmp_files)
        
        return redirect(url_for('main.home'))
    else:
        return redirect(url_for('main.home'))

@main.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    
    tmp_files = []
    print(request.method)
    # handle audio upload from Dropzone
    if 'upload' in request.url and request.method == 'POST':
        raw_files = request.files
        
        for raw_file in raw_files:
            tmp_dict = {}
            file = request.files.get(raw_file) 
            name = api_utils.valid_name(file.filename) 
            filename = audios_.save(
                file,
                name=name   
            )

            api_utils.load_and_save(os.path.join(current_app.config['UPLOADED_AUDIOS_DEST'],filename))
            filename,ext = os.path.splitext(filename)
            tmp_dict.update({'name':filename,
                             'url':os.path.split(request.url)[0] +'/_uploads/audios/' +filename+ext,
                             'ext':ext,
                             'LANGUAGE_ID':{'UNKNOWN':0.0}})
            t = time.process_time()
            #tmp_dict.update(check_audio_for_minors(
            #    app.config['UPLOADED_AUDIOS_DEST'] + "/" + filename)[0])
            elapsed_time = time.process_time() - t
            flash('[INFO] INFERENCE TIME: ',elapsed_time)
            tmp_files.append(tmp_dict)
       
        add_to_table(tmp_files)
    
    # return dropzone template on GET request    
    return render_template('pages/upload.html', active='upload')

@main.route('/results')
@login_required
def results():
    # Pagination
    idx = request.args.get('page',1, type=int)
    db_size = collection.count_documents({})
    pag = pagination(idx=idx, db_size=db_size, url='main.results') 
    context.update(pag)
    
    return render_template(
        'pages/files.html',
        db_size=db_size,
        current=idx,
        **context,
    )

@main.route('/submit_form', methods=['POST'])
@login_required
def submit_form():
    if request.method =='POST':
        checked_list = request.form.getlist('audios')
        languages = request.form.getlist('language')
        ids = request.form.getlist('ids')
        tmp_dict = dict(zip(ids,languages))
        
        if request.form['action']=='Submit':
            for name in checked_list:
                language = tmp_dict[name]
                if language:
                    data = dict(name=name, 
                            LANGUAGE_ID={language: 1.0})
                    add_to_table(data)
                    flash(f'FILE {name} updated to {language}')
                
        elif request.form['action']=='Delete':
            delete_from_table(checked_list)
        elif request.form['action'].upper() in ['VAD','DENOISE','LID','GID','ASR','ALL']:
            if request.form['action'].upper() in ['VAD','DENOISE','ALL']:
                entries = collection.find({'name': {'$in':checked_list}})
                heavy_process = Process(  # Create a daemonic process with heavy "my_func"
                                target=denoiser_request,
                                args=(entries,request),
                            daemon=True
                                )
                heavy_process.start()

            if request.form['action'].upper() in ['LID','GID','ASR','ALL']:
                entries = collection.find({'name': {'$in':checked_list}})
                heavy_process = Process(  # Create a daemonic process with heavy "my_func"
                                target=speechapp_request,
                                args=(entries,request),
                                daemon=True
                                )
                heavy_process.start()

        else:
            flash('Invalid Action')
            
        return redirect(url_for('main.home'))
    return redirect(url_for('main.home'))

@main.route('/post_file', methods=['POST'])
def post_file():
    if request.method == 'POST':
        prefix, infer_mode = api_utils.verify_request(request)
        data = request.data

        key = request.args.get('key')

        if not verify_key(key):
            return abort(403, 'Not allowed, verify your api_key')

        filename,ext = api_utils.save_request(data,prefix)

        tmp_files = {}
        tmp_files.update({'name':filename,
                        'url':os.path.join(os.path.split(request.url)[0],
                                        f'_uploads/audios/{filename}{ext}'),
                        'LANGUAGE_ID':{'UNKNOWN':0.0},
                        'ext':ext})
        

        
        if infer_mode in ['vad','denoiser']:
            api_utils.load_audio(os.path.join(
                current_app.config['UPLOADED_AUDIOS_DEST'],filename+ext))
            vad_segments, vad_audio, clean, noise_audio = api_utils.process_vad_denoiser(save=False,
                                            model_uri=current_app.config['TORCHSERVE_URI'],
                                            infer_mode=infer_mode.lower())
            output = {'vad_segments':vad_segments, 
                'vad_audio':vad_audio.tolist(), 
                'clean':clean.tolist(), 
                'noise_audio':noise_audio.tolist()}

            tmp_files.update({'vad_segments':vad_segments})
        
        elif infer_mode in ['asr', 'lid', 'gid']:
            output = api_utils.request_speechapp(audio=data,
                                        speechapp_uri=current_app.config['SPEECHAPP_URI'],
                                        infer_mode=infer_mode)
            output = {output['prediction']:output['confidence']}
            if infer_mode == 'lid':
                tmp_files.update({'LANGUAGE_ID':output })
            elif infer_mode == 'gid':
                tmp_files.update({'GENDER':output})
            else:
                tmp_files.update({'TEXT':output})

        add_to_table(tmp_files)

        
        return output

def verify_key(key):
    find_user = User.get_by_apikey(key)
    if find_user:
        login_user(find_user)
        return True
    return False

def denoiser_request(entries, request):
    for entry in entries:
        api_utils.load_audio(os.path.join(
            current_app.config['UPLOADED_AUDIOS_DEST'],entry['name']+entry['ext']))
        _,_,webrtc_name = api_utils.webrtc_vad_audio(save=True)
        
        infer_mode = 'vad' if request.form['action']=='VAD' else 'denoiser'
        paths, vad_segments = api_utils.process_vad_denoiser(save=True,
                                        model_uri=current_app.config['TORCHSERVE_URI'],
                                        infer_mode=infer_mode)

        webrtc_url = os.path.join(os.path.split(request.url)[0],
                                    f'_uploads/audios/{webrtc_name}')

                                    
        data = dict(name=entry['name'],
                    webrtc=webrtc_name,
                    webrtc_url=webrtc_url,
                    vad_segments=vad_segments)

        for name, path in paths.items():
            url = os.path.join(os.path.split(request.url)[0],
                            f'_uploads/audios/{path[1]}')
                                    
            data.update({f'{name}':f'{path[1]}',
                            f'{name}_url':f'{url}'})

        add_to_table(data)
        flash(f'file processed {entry["name"]}')
    flash('ALL DATA PROCESSED')

def speechapp_request(entries, request):
    for entry in entries:
        filepath= os.path.join(
            current_app.config['UPLOADED_AUDIOS_DEST'],entry['name']+entry['ext'])
        
        action = request.form['action'].lower()
        infer_modes = ['lid','gid','asr'] if action == 'all' else [action]

        with open(filepath, 'rb') as file_:
            file_ = file_.read()

            for infer_mode in infer_modes:
                data = dict(name=entry['name'])
                output = api_utils.request_speechapp(audio=file_,
                                            speechapp_uri=current_app.config['SPEECHAPP_URI'],
                                            infer_mode=infer_mode)
                output = {output['prediction'].replace('\\','').replace('\'',''):output['confidence']}

                if infer_mode == 'lid':
                    data.update({'LANGUAGE_ID':output })
                elif infer_mode == 'gid':
                    data.update({'GENDER':output})
                else:
                    data.update({'TEXT':output})
                
                
                add_to_table(data)
                flash(f'file processed {entry["name"]}')
        
    flash('ALL DATA PROCESSED')

def pagination(idx:int=1, db_size=0, sample_length:int=12, url='main.home'):
    
    total_pages = math.ceil(db_size/float(sample_length))
    
    if idx > total_pages:
        idx = total_pages
        
    next_url = url_for(url, page=idx+1) if sample_length*(idx) < db_size else None
    prev_url = url_for(url, page=idx-1) if sample_length*(idx-1) > 0 else None
    
    im_to = int(sample_length)*idx if next_url is not None else db_size
    
    offset = (idx-1)*sample_length if idx -1 >=0 else 0
    sample = list(collection.find().skip(offset).limit(sample_length))
    for item in sample:
        del item['_id']

    return {'total_pages':total_pages,
            'next_url':next_url,
            'prev_url':prev_url,
            'im_to':im_to,
            'im_from':offset+1 if im_to else 0,
            'offset':offset, 
            'result':sample,
            'active':url.split('.')[-1]}


def delete_from_table(elements:list=[]):
    if not len(elements):
        flash(f'NO ENTRIES SELECTED')
        return
    ids = collection.find({'name': {'$in':elements}})
    names = [el['name']+el['ext'] for el in ids]
    x = collection.delete_many({'name': {'$in':elements}})
    delete_audios(names)
    flash(f'{x.deleted_count} FILE(S) DELETED') 

def reset_table(full=True):
    global collection
    global tmp_memory
    if full:
        x = collection.delete_many({})
        collection = db.audio
        delete_audios()
        flash(f'All {x.deleted_count} entries deleted')
        tmp_memory = []
    else:
        ids = collection.find({'_id': {'$in':tmp_memory}})
        names = [el['name']+el['ext'] for el in ids]
        delete_audios(names)
        x = collection.delete_many({'_id': {'$in':tmp_memory}})
        flash(f'{x.deleted_count} entries deleted')
        tmp_memory = []

def delete_audios(names:list=None):
    if not isinstance(names,list): 
        names = os.listdir(os.path.join(current_app.static_folder, "tmp/uploads"))
        names = filter(lambda x: x != '.keep', names)
    for name in names:
        os.remove(os.path.join(current_app.static_folder, "tmp/uploads",name))

def add_to_table(elements):
    global tmp_memory 
    if isinstance(elements, list):
        for element in elements:
            add_to_table(element)
    elif isinstance(elements, dict):
        name = elements.pop('name',None)
        elements.update({'user_id':current_user._id})
        if name is None:
            flash(f'No name in your entry for db')
            return
        id_ = collection.update_one({'name':name},
            {'$set':elements},upsert=True).upserted_id
        tmp_memory.append(id_)
    else:
        flash(f'Impossible to add value {elements} to table')
    #print_table()

        
def print_table():
    print('TABLE BEGIN')
    for element in collection.find({}):
        print(element)
    print('TABLE END')

# if __name__ == '__main__':
#     app.run(debug=True, threaded=False, host='0.0.0.0', port=5605)
