from flask_pymongo import PyMongo
from flask_login import LoginManager
from flask_uploads import UploadSet, configure_uploads, AllExcept, SCRIPTS,EXECUTABLES

mongo = PyMongo()
login_manager = LoginManager()
audios_ = UploadSet('audios', AllExcept(SCRIPTS + EXECUTABLES))