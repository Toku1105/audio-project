from flask import Blueprint, session, flash, request, render_template, redirect, url_for, current_app
from flask_login import login_user, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from .extensions import mongo, login_manager
from .models import User


auth = Blueprint('auth', __name__)

@auth.route('/login', methods=['POST','GET'])
def login():
	

	if current_user.is_authenticated:
		flash('User already logged in')
		return redirect(url_for("main.home"))

	if request.method == "POST":
		name = request.form.get("username")
		password = request.form.get("password")
		remember = True if request.form.get('remember') else False
		find_user = User.get_by_username(name)

		if not User.login_valid(name, password):
			flash('Please check your login details and try again.')
			return redirect(url_for("auth.login"))
		find_user.api_register(name)
		login_user(find_user, remember=remember)
		session.permanent = True
		
		flash(f'Logged in as {name}')	
		return redirect(url_for('main.home'))
	flash('Please login to your account')		
	return render_template("pages/login.html",active='login')

@auth.route('/register',methods=['POST','GET'])
def register():
	if current_user.is_authenticated:
		flash('Logout to register new user')
		return redirect(url_for("main.home"))

	if request.method == "POST":
		name = request.form.get("username")
		
		password1 = request.form.get("pass1")
		password2 = request.form.get("pass2")
		
		name_found = User.get_by_username(name)

		if name_found:
			flash('Username already exists')
			return redirect(url_for("auth.register"))

		if password1 != password2:
			flash('Passwords should match!')
			return redirect(url_for("auth.register"))

		else:
			User.register(name,password2)
			flash(f'Account created')

			return redirect(url_for("auth.login"))
	return render_template("pages/register.html",active='register')

@auth.route('/logout', methods=["POST", "GET"])
def logout():
	User.api_unregister(current_user.username)
	logout_user()
	return redirect(url_for("auth.login"))

@login_manager.user_loader
def load_user(user_id):
	user = User.get_by_id(user_id)
	if user:
		return user
	return None

@current_app.context_processor
def inject_user():

    return dict(
    		g_username=current_user.username if current_user.is_authenticated else None,
    		g_user_autenticated=current_user.is_authenticated,
			g_user_api_key=current_user.api_key if current_user.is_authenticated else None
    	)