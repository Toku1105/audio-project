
import os 
from .request_processing import Audio 
from flask import current_app
import numpy as np 
import logging
from .torch_utils import plot_waveform
import requests
logger = logging.getLogger('develop')
ap = Audio(16000)

def load_audio(filepath): 
    ap.read(filepath)
    return True

def load_and_save(filepath):
    ap.read(filepath, upload=True)
    path = ap.save_audio()
    return path

def load_audio_request(filepath):
    with open(filepath, 'rb') as file_:
        file_ = file_.read()
    return file_

def verify_request(request):
    filename = request.args.get('name')
    if filename is None:
        filename  = 'api_upload_.wav'
        print(f'name not assigned in arguments, assigning {filename}')

    if '/' in filename:
        abort(400, "no subdirectories directories allowed")

    if not 'wav' in filename:
        abort(400, "Only .wav files are allowed")
    
    filename = os.path.splitext(filename)[0] 
    infer_mode = request.args.get('action').lower()
    if not infer_mode or not infer_mode in ['vad','denoiser','lid','gid','asr']:
        return abort(404, f'action {infer_mode} not found, use lid,gid, or asr')
    
    return filename, infer_mode

def webrtc_vad_audio(save=False):
    segments = ap.webrtc_vad(ap.audio[0], aggressiveness=3, window_duration=0.010)
    seg_sample = ap.cutter(ap.audio[0],segments)
    if save:
        filepath, ext = os.path.splitext(ap.filepath)
        path, name = ap.save_audio(data=seg_sample,
                      path=filepath+'_webrtc.wav',
                      sr=ap.sr)
    return (seg_sample,path,name)

def request_speechapp(audio:np.ndarray,
                        speechapp_uri:str,
                        infer_mode:str):
    headers = {"Content-Type": "audio/wav",}
    params = {"uploadType": "media", 
                "action":infer_mode,}
    url = speechapp_uri
    r = requests.post(url, params=params, headers=headers, data=audio)
    return r.json()


def process_vad_denoiser(save=False, model_uri=None, infer_mode='vad'):
    vad_segments, vad_audio, clean, noise_audio = ap.deep_vad_denoiser(ap.audio[0], model_uri, infer_mode=infer_mode)
    paths = {}
    filepath, ext = os.path.splitext(ap.filepath)
    if infer_mode == 'denoiser' and save:
        # path, name = ap.save_audio(data=noise_audio,
        #               path=filepath+'_noise.wav',
        #               sr=ap.sr)
        # paths.update({'noise':[path,name]})

        # path, name = plot_waveform(np.asarray(clean),
        #         16000,
        #         title=None,
        #         save_path=filepath+'_denoiser.png')
        # paths.update({'denoiser_waveform':[path,name]})
        
        path, name = ap.save_audio(data=clean,
                      path=filepath+'_denoised_vad.wav',
                      sr=ap.sr)
        paths.update({'denoised_vad':[path,name]})
    
    if save:
        # path, name = plot_waveform(ap.audio[0],
        #             16000,
        #             title=None,
        #             save_path=filepath+'_vad.png',
        #             segments=vad_segments)
        # paths.update({'vad_waveform':[path,name]})
        path, name = ap.save_audio(data=vad_audio,
                        path=filepath+'_deep_vad.wav',
                        sr=ap.sr)
        paths.update({'deepvad':[path,name]})

        return paths, vad_segments

    return vad_segments, vad_audio, clean, noise_audio

def save_audio(data,prefix='upload_'):
    ''' Save Audio file from dropzone upload'''
    
    i = 0 if os.path.exists(os.path.join(current_app.config['UPLOADED_AUDIOS_DEST'],f"{prefix}.wav")) else ''
    while os.path.exists(os.path.join(current_app.config['UPLOADED_AUDIOS_DEST'],
                                          f"{prefix}{i}.wav")):
            i += 1
    
    with open(os.path.join(current_app.config['UPLOADED_AUDIOS_DEST'],
                                      f"{prefix}{i}.wav"), 'wb') as audio:
        
        data.save(audio)
    print(f'saved {prefix}{i}.wav')
 
    return os.path.splitext(f'{prefix}{i}.wav')

def save_request(data,prefix='upload_'):
    ''' Save binary data sent by POST'''
    i = 0 if os.path.exists(os.path.join(current_app.config['UPLOADED_AUDIOS_DEST'],f"{prefix}.wav")) else ''
    while os.path.exists(os.path.join(current_app.config['UPLOADED_AUDIOS_DEST'],
                                          f"{prefix}{i}.wav")):
            i += 1
    
    with open(os.path.join(current_app.config['UPLOADED_AUDIOS_DEST'],
                                      f"{prefix}{i}.wav"), 'wb') as audio:
        
        audio.write(data)
    print(f'saved {prefix}{i}.wav')
 
    return os.path.splitext(f'{prefix}{i}.wav')

def valid_name(filename:str):
    name,ext = os.path.splitext(filename)
    name = name.replace('.','_').replace(' ','')
    return name+ext