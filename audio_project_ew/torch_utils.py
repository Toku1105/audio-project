import torch
import torch.nn as nn 
import torchaudio.transforms as T
import librosa
from matplotlib import pyplot as plt
import os 

class TrainClock(object):
    """ Clock object to track epoch and step during training
    """
    def __init__(self, epoch=0, minibatch=0, step=0):
        self.epoch = epoch
        self.minibatch = minibatch
        self.step = step

    def tick(self):
        self.minibatch += 1
        self.step += 1

    def tock(self):
        self.epoch += 1
        self.minibatch = 0

    def make_checkpoint(self):
        return {
            'epoch': self.epoch,
            'minibatch': self.minibatch,
            'step': self.step
        }
    @classmethod
    def restore_checkpoint(cls, clock_dict):
        return cls(**clock_dict)

class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self, name):
        self.name = name
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def get_default_device():
    """Pick GPU if available, else CPU"""
    if torch.cuda.is_available():
        return torch.device('cuda')
    else:
        return torch.device('cpu')

def to_parallel(model):
    """ Verify is there are multiple gpu devices and wrap with DataParallel
            Args:
                model(torch.model): model to parallelize
            Return:
                model(torch.model): nn.DataParallel(model)
    """
    if torch.cuda.device_count() > 1:
        print(f'{torch.cuda.device_count()} GPUs available')
        model = nn.DataParallel(model)
    return model 

def to_device(data, device):
    """Move tensor(s) to chosen device
    Args: 
        data(Tensor,lost,tuple): data to move
        device(torch.device): objective device\
    Return:
        data: data into device
    """
    if isinstance(data, (list,tuple)):
        return [to_device(x, device) for x in data]
    return data.to(device, non_blocking=True)

def to_tensor(data):
    """ Convert list or array to torch.Tensor
        Args: 
            data(np.ndarray, list)
        Return:
            data(torch.Tensor(float))
    """
    tn = torch.FloatTensor(data)
    return tn

def step_loader(iterable):
    """ Convert Iterable into generator
        Args:
            Iterable(DataLoader)
        Return:
            Step of iterable
    """
    while True:
        for x in iterable:
            yield x

def get_spectrogram(
    waveform,
    n_fft = 400,
    win_len = None,
    hop_len = None,
    power = 2.0,
):
    """ Generate Spectrogram from audio signal
        Args:
            waveform(np.ndarray, torch.Tensor): batch of audios to convert to Spectrogram (batch_size,channels,audio_length)
            n_fft(int): fft value for stft
            win_len(int=None default): length of slidding windows for stft
            hop_len(int): length of hop for stft
            power(int): power for spectrogram (0-Complex), (1-energy), (2-power), etc
        Return:
            spec(torch.Tensor): Tensor of batch of spectrograms
    """
    spectrogram = T.Spectrogram(
      n_fft=n_fft,
      win_length=win_len,
      hop_length=hop_len,
      center=True,
      pad_mode="reflect",
      power=power 
    )
    return spectrogram(waveform)

def complex_element_wise_w_logit(Y,signal2_sigmoid):
    """ Element-wise product of complex signals
        Args: 
            Y(torch.Tensor(batch,2,h,w)): First element to multiply 2 layers real-imaginary
            signal2_sigmoid(torch.Tensor(batch,2,h,w)): Element to apply logit and multiply with Y
        Return:
            torch.Tensor: Element wise multiplication of complex signals
    """
    M = torch.logit(signal2_sigmoid, eps=1e-6)
    r = M[:, 0, :, :] * Y[:, 0, :, :] - M[:, 1, :, :] * Y[:, 1, :, :]
    i = M[:, 0, :, :] * Y[:, 1, :, :] + M[:, 1, :, :] * Y[:, 0, :, :]
    return torch.stack([r, i], dim=1)

def element_wise_w_logit(Y,signal2_sigmoid, norm = True):
    """ Element-wise product of complex signals
        Args: 
            Y(torch.Tensor(batch,2,h,w)): First element to multiply 
            signal2_sigmoid(torch.Tensor(batch,2,h,w)): Element to apply logit and multiply with Y
        Return:
            torch.Tensor: Element wise multiplication of complex signals reescaled to [-1.,1]
    """
    M = torch.logit(signal2_sigmoid, eps=1e-6)
    spec = Y*M
    if norm:
        shape = spec.size()
        spec = spec.view(spec.size(0), -1)
        mini = spec.min(1, keepdim=True)[0] + 1e-11
        
        maxi = spec.max(1, keepdim=True)[0]
        spec = -1. +2. * (spec - mini)/(maxi - mini)
        spec = spec.view(shape)
    return spec

class DeviceDataLoaderVAD():
    """Wrap a dataloader to move data to a device"""
    def __init__(self, dl, device):
        self.dl = dl
        self.device = device
        
    def __iter__(self):
        """Yield a batch of data after moving it to device"""
        for b in self.dl:
            batch = (b.get('audio',torch.Tensor()),b.get('category',torch.Tensor()),b.get('segments',torch.Tensor()),b.get('noisy_audio',torch.Tensor()),b.get('noise',torch.Tensor()))
            yield to_device(batch, self.device)

    def __len__(self):
        """Number of batches"""
        return len(self.dl)

class DeviceDataLoaderNoise():
    """Wrap a dataloader to move data to a device"""
    def __init__(self, dl, device):
        self.dl = dl
        self.device = device
        
    def __iter__(self):
        """Yield a batch of data after moving it to device"""
        for b in self.dl:
            batch = (b.get('audio',torch.Tensor()),
                    b.get('noise',torch.Tensor()),
                    b.get('noise_profile',torch.Tensor()),
                    b.get('noisy_audio',torch.Tensor()))
            yield to_device(batch, self.device)

    def __len__(self):
        """Number of batches"""
        return len(self.dl)


class StatsRecorder:
    def __init__(self, red_dims=(0,2,3)):
        """Accumulates normalization statistics across mini-batches.
        ref: http://notmatthancock.github.io/2017/03/23/simple-batch-stat-updates.html
        """
        self.red_dims = red_dims # which mini-batch dimensions to average over
        self.nobservations = 0   # running number of observations

    def update(self, data):
        """
        data: ndarray, shape (nobservations, ndimensions)
        """
        # initialize stats and dimensions on first batch
        if self.nobservations == 0:
            self.mean = data.mean(dim=self.red_dims, keepdim=True)
            self.std  = data.std (dim=self.red_dims,keepdim=True)
            self.nobservations = data.shape[0]
            self.ndimensions   = data.shape[1]
        else:
            if data.shape[1] != self.ndimensions:
                raise ValueError('Data dims do not match previous observations.')
            
            # find mean of new mini batch
            newmean = data.mean(dim=self.red_dims, keepdim=True)
            newstd  = data.std(dim=self.red_dims, keepdim=True)
            
            # update number of observations
            m = self.nobservations * 1.0
            n = data.shape[0]

            # update running statistics
            tmp = self.mean
            self.mean = m/(m+n)*tmp + n/(m+n)*newmean
            self.std  = m/(m+n)*self.std**2 + n/(m+n)*newstd**2 +\
                        m*n/(m+n)**2 * (tmp - newmean)**2
            self.std  = torch.sqrt(self.std)
                                 
            # update total number of seen samples
            self.nobservations += n

def plot_waveform(waveform, sample_rate, title="Waveform", xlim=None, ylim=None, save_path=None):
    #waveform = waveform.numpy()

    num_channels, num_frames = waveform.shape
    time_axis = torch.arange(0, num_frames) / sample_rate

    figure, axes = plt.subplots(num_channels, 1)
    if num_channels == 1:
        axes = [axes]
    
    for c in range(num_channels):
        axes[c].plot(time_axis, waveform[c], linewidth=1)
        axes[c].grid(True)
        if num_channels > 1:
            axes[c].set_ylabel(f'Channel {c+1}')
        if xlim:
            axes[c].set_xlim(xlim)
        if ylim:
            axes[c].set_ylim(ylim)
    figure.suptitle(title)
    if save_path:
        plt.savefig(save_path, transparent=True)
        return os.path.split(save_path)
    plt.show(block=False)

def reconstruct_from_spec(
    spec,
    n_fft = 512,
    win_len = 400,
    hop_len = 160,
):
    griffin_lim = T.GriffinLim(
    n_fft=n_fft,
    win_length=400,
    hop_length=160,
    power=2
    )
    return griffin_lim(spec)

def plot_spectrogram(spec, title=None, ylabel='freq_bin', aspect='auto', xmax=None):
    fig, axs = plt.subplots(1, 1)
    axs.set_title(title or 'Spectrogram (db)')
    axs.set_ylabel(ylabel)
    axs.set_xlabel('frame')
    im = axs.imshow(librosa.power_to_db(spec), origin='lower', aspect=aspect)
    if xmax:
        axs.set_xlim((0, xmax))
    fig.colorbar(im, ax=axs)
    plt.show(block=False)