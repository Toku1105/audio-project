from ts.torch_handler.base_handler import BaseHandler
from abc import ABC
import io
import base64
import torch
import numpy as np
from captum.attr import IntegratedGradients
import os
import importlib.util
import inspect
import json
import logging
import gzip
import torchaudio.transforms as T
logger = logging.getLogger(__name__)
os.environ["CUDA_VISIBLE_DEVICES"] = '1'



class CustomHandler(BaseHandler, ABC):
    """
    Base class for handlers
    """


    def initialize(self, context):
        print(context.system_properties)
        super().initialize(context)
        self.ig = IntegratedGradients(self.model)
        self.initialized = True

    def _load_pickled_model(self, model_dir, model_file, model_pt_path):
        """
        Loads the pickle file from the given model path.
        Args:
            model_dir (str): Points to the location of the model artefacts.
            model_file (.py): the file which contains the model class.
            model_pt_path (str): points to the location of the model pickle file.
        Raises:
            RuntimeError: It raises this error when the model.py file is missing.
            ValueError: Raises value error when there is more than one class in the label,
                        since the mapping supports only one label per class.
        Returns:
            serialized model file: Returns the pickled pytorch model file
        """
        model_def_path = os.path.join(model_dir, model_file)
        if not os.path.isfile(model_def_path):
            raise RuntimeError("Missing the model.py file")

        module = importlib.import_module(model_file.split(".")[0])
        classes = [cls[1] for cls in inspect.getmembers(module, lambda member: inspect.isclass(member) and member.__module__ == module.__name__)]
        
        model_class = None
        for idx, cl in enumerate(classes):
            if cl.__name__=='FullDenoiserModel':
                logger.info('DENOISER classes done')
                model_class = classes[idx]

        if model_class is None:
            raise RuntimeError("Model Class not loaded")

        model = model_class()
        if model_pt_path:
            logger.info(f'DENOISER STATE_DIC to {self.device}')
            state_dict = torch.load(model_pt_path, map_location=self.device)
            logger.info('DENOISER statedic loaded')
            model.load_state_dict(state_dict['model_state_dict'])
            logger.info('DENOISER model loaded')
        return model
        
    def preprocess(self, data):
        """The preprocess function of MNIST program converts the input data to a float tensor
        Args:
            data (List): Input data from the request is in the form of a Tensor
        Returns:
            list : The preprocess function returns the input image as a list of float tensors.
        """
        # Compat layer: normally the envelope should just return the data
        # directly, but older versions of Torchserve didn't have envelope.
        d_input = data[0].get("data") or data[0].get("body")

        for key, val in d_input.items(): 
            if isinstance(val, list): 
                d_input[key] =  np.asarray(val)
                print(d_input[key].shape)

        input_ = self._input_processing(d_input)
        

        return input_

    def inference(self, data):
        noise_profiles, noisy_spec = data
        with torch.no_grad():
            inference_output = self.model(noisy_spec, noise_profiles)
        return inference_output

    def reconstruct_from_spec(self,
        spec,
        n_fft = 512,
        win_len = 400,
        hop_len = 160,
    ):  
        griffin_lim = T.GriffinLim(
        n_fft=n_fft,
        win_length=400,
        hop_length=160,
        power=2,
        )
        return griffin_lim(spec)

    def postprocess(self, inference_output):
        noise_pred, audio_mask_pred, audio_recovered = inference_output
        print(noise_pred.shape, audio_mask_pred.shape, audio_recovered.shape)
        
        noise_pred =  noise_pred.cpu().detach()
        audio_mask_pred = audio_mask_pred.squeeze(1).permute(1,0,2).reshape((257,-1)).cpu().detach().numpy()
        audio_recovered = audio_recovered.squeeze(1).permute(1,0,2).reshape((257,-1)).cpu().detach()
        audio_recovered = self.reconstruct_from_spec(audio_recovered).numpy()
        noise_pred = self.reconstruct_from_spec(noise_pred).numpy()


        output = {'recovered_audio_spec':audio_recovered.tolist(), 'noise_pred_spec':noise_pred.tolist()}
        return [gzip.compress(json.dumps(output).encode('utf-8'))]#self.dict_to_json(output)

    def dict_to_json(self,dic:dict):
        for key, val in dic.items(): 
            if isinstance(val, np.ndarray):
                memfile = io.BytesIO()
                np.save(memfile, val)
                memfile.seek(0)
                encoded = memfile.read().decode('latin-1')
                dic[key] = encoded
        return json.dumps(dic)

    def segments_to_dict(self, segments:np.ndarray,):
        ''' Get segments from sigmoid output and convert array to webrtc output format 
            Args:
                segments(np.ndarray): probabilities of segment being speech or not
                sampling_rate(int): sampling rate default: 16000
                windows_duration(float): preferable 0.020
                threshold(float): [0-1] value to decide if segments is speech or not given probability 
            Returns:
                audio(np.ndarray): segmented array
        '''
        sampling_rate:int=16000
        step = int(sampling_rate * 0.020)
        
        return [{'start':idx*step, 
            'stop':idx*step+step, 
            'is_speech': 'true' if seg>0.5 else 'false'} for idx, seg in enumerate(segments)]
            
    def _input_processing(self, input_:dict)->tuple:
        noise_profiles = input_['noise_profiles']
        noise_profiles = torch.FloatTensor(noise_profiles).unsqueeze(1)
        noisy_spec = input_['specs']
        noisy_spec = torch.FloatTensor(noisy_spec)
        return (noise_profiles.to(self.device), noisy_spec.to(self.device))

    
    def _verify_input(self, audio:np.ndarray)->np.ndarray:
        """ Verify is a single channel audio or batch of single channel audios"""
        if len(audio.shape)>2:
            raise Exception('Input should be 1D or 2D for batches of audio')
        return np.expand_dims(audio, axis=0) if len(audio.shape)==1 else audio

