from collections import OrderedDict
from .torch_utils import (TrainClock, 
                         get_default_device, 
                         AverageMeter, 
                         to_parallel, 
                         to_device,
                         complex_element_wise_w_logit,
                         element_wise_w_logit)
from .utils import makedirs
from .model import VoiceActivityDetector, FullDenoiserModel, HelperDenoiserSpec
from pytorch_msssim import ssim, ms_ssim, SSIM, MS_SSIM
from tensorboardX import SummaryWriter
import torch.optim as optim
import torch.nn as nn 
import torch
import tqdm
import os
import numpy as np 

class BaseWorker(object):
    def __init__(self, config, device = None):
        self.logdir = os.path.join(config['logdir'], config['name'])
        makedirs(self.logdir)
        self.modeldir = os.path.join(config['modeldir'], config['name'])
        makedirs(self.modeldir)
        self.batch_size = config['batch_size']
        self.lr = config['lr']
        self.lr_decay = config['lr_decay']
        self.lr_step_size = config['lr_step_size']
        self.clock = TrainClock()
        self.device = get_default_device() if device is None else device
        self.resume = config['resume']
        self.model = self.model_build()
        if isinstance(self.model, nn.DataParallel):
            self.model_attr_accessor = self.model.module
        else:
            self.model_attr_accessor = self.model
        self.criterion = self.set_loss_function()
        self.warm_restarts = config['warm_restarts']
        self.optimizer = self.set_optimizer(self.model)
        self.scheduler = self.set_scheduler(self.optimizer)
        
        # set tensorboard writer
        self.train_tb = SummaryWriter(os.path.join(self.logdir, 'train.events'))
        self.val_tb = SummaryWriter(os.path.join(self.logdir, 'val.events'))
        self.add_graph()

    def add_graph(self,):
        """Add graph to tensorboard """
        raise NotImplementedError("add_graph method not implemented")

    def model_build(self,):
        """Build Model to train"""
        raise NotImplementedError("model_build method not implemented")
    
    def forward(self,):
        """Step for training"""
        raise NotImplementedError("forward method not implemented")
    
    def update_network(self, loss:dict, clip=None):
        """update network by back propagation"""
        loss = sum(loss.values())
        self.optimizer.zero_grad()
        loss.backward()
        if clip:
            nn.utils.clip_grad_norm_(self.model.parameters(), clip)
        self.optimizer.step()
        
    
    def update_learning_rate(self):
        """record and update learning rate"""
        self.train_tb.add_scalar('learning_rate', self.optimizer.param_groups[-1]['lr'], self.clock.epoch)
        if not self.warm_restarts:
            self.scheduler.step()
        self.clock.tock()
        
    def record_losses(self, loss, mode='TRAIN'):
        # record loss to tensorboard
        tb = self.train_tb if mode == 'TRAIN' else self.val_tb
        with tb:
            for loss_name, loss_val in loss.items():
                tb.add_scalar(loss_name, loss_val.item(), self.clock.step)

    def set_loss_function(self,):
        """ Loss function using for training """
        return to_device(nn.CrossEntropyLoss(),self.device)
    
    def set_optimizer(self, model):
        """set optimizer and lr scheduler used in training"""
        return optim.Adam(model.parameters(), self.lr)

    def set_scheduler(self, opt):
        """set lr scheduler used in training"""
        return optim.lr_scheduler.ExponentialLR(opt, self.lr_decay)
    
    def step_train(self, data, clip=None):
        """step for training"""
        self.model.train()

        output, loss = self.forward(data)

        self.update_network(loss, clip)
        self.record_losses(loss,'TRAIN')
        return output, loss
    
    def step_test(self, data, record=True):
        """step for training"""
        self.model.eval()
        with torch.no_grad():
            output, loss = self.forward(data)
        if record:
            self.record_losses(loss,'VAL')
        return output, loss

    def save_ckpt(self, name=None):
        """save checkpoint during training for future restore"""
        if name is None:
            save_path = os.path.join(self.modeldir, f"ckpt_epoch{self.clock.epoch}.pth")
            print(f"Checkpoint saved at {save_path}")
        else:
            save_path = os.path.join(self.modeldir, f"{name}.pth")
        
        torch.save({
            'clock': self.clock.make_checkpoint(),
            'model_state_dict': self.model_attr_accessor.cpu().state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
            'scheduler_state_dict': self.scheduler.state_dict(),
        }, save_path)

        print(f'Checkpoint saved at {self.clock.make_checkpoint()}, name:{name}')

        self.model.cuda()
        
    def load_ckpt(self, name=None, epoch=None):
        """load checkpoint from saved checkpoint"""
        name = name if name else f"ckpt_epoch{epoch}"
        load_path = os.path.join(self.modeldir, f"{name}.pth")
        if not os.path.exists(load_path):
            raise ValueError(f"Checkpoint {load_path} not exists.")

        # checkpoint = torch.load(load_path)
        # state_dict = self.model.module.state_dict()
        # print(f"Checkpoint loaded from {load_path}")
        # for name, param in self.model.module.named_parameters():
        #     if name in state_dict.keys():
        #         print(name)
        #         state_dict[name] = checkpoint['model_state_dict'][name]
                
        checkpoint = torch.load(load_path)
        print(f"Checkpoint loaded from {load_path}")
        self.model_attr_accessor.load_state_dict(checkpoint['model_state_dict'])

        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.scheduler.load_state_dict(checkpoint['scheduler_state_dict'])
        
        self.optimizer = self.set_optimizer(self.model)
        self.scheduler = self.set_scheduler(self.optimizer)
        self.clock = TrainClock.restore_checkpoint(checkpoint['clock'])
        print(f'Restored-> Opt: {self.optimizer}, Scheduler: {self.scheduler}, Epoch: {self.clock.epoch}')
        
        
class VADWorker(BaseWorker):
    def __init__(self,config,device=None):
        self.validation_step_freq = config['validation_step_freq']
        super().__init__(config,device)
    
    def add_graph(self,):
        if not self.resume:
            with self.train_tb:
                self.train_tb.add_graph(self.model_attr_accessor, to_device(torch.randn((1,1,32000)),self.device))

    def model_build(self):
        """Build model for VAD"""
        model = VoiceActivityDetector()
        if self.device != torch.device('cpu'):
            model = to_parallel(model)
        model = to_device(model,self.device)
        print(f'Model in device:{self.device}')
        return model 
    
    def set_loss_function(self):
        """set loss function used in training for VAD"""
        return to_device(nn.BCEWithLogitsLoss(),self.device)
    
    def set_optimizer(self, model):
        """set optimizer and lr scheduler used in training of VAD"""
        return optim.SGD(model.parameters(), lr=self.lr, momentum=0.9, weight_decay=1e-6 )

    def set_scheduler(self, opt, steps=2, mult=2):
        """set lr scheduler used in training of VAD"""
        if self.warm_restarts:
            scheduler = optim.lr_scheduler.CosineAnnealingWarmRestarts(opt,
                                                                T_0=steps,
                                                                T_mult=mult,
                                                                verbose=True)
        else:
            scheduler = optim.lr_scheduler.StepLR(opt, self.lr_step_size)
        return scheduler
        
    
    def forward(self, data):
        # Batch order in DeviceDataLoader
        # data[0] audio
        # data[1] category
        # data[2] segments
        # data[3] noisy audio
        # data[4] noise
        
        label = data[2]
        audio = data[3]
        v_num_frames = int(audio.shape[-1]/(16000*0.02))
        output,_ = self.model(audio,v_num_frames=v_num_frames)
        loss = self.criterion(output, label)

        return output, {'bce_loss': loss}
    
    def train_batch(self,train_loader, test_loader_step, epoch):
        iters = len(train_loader)
        pbar = tqdm.tqdm(train_loader)
        metrics = AverageMeter("loss")
        epoch_acc = AverageMeter("acc")

        for batch, data in enumerate(pbar):
            output, loss = self.step_train(data)
            pbar.set_description(f"EPOCH[{self.clock.epoch}]")
            pbar.set_postfix(OrderedDict({k: v.item() for k, v in loss.items()}))
            pred_labels = (torch.sigmoid(output).detach().cpu().numpy() >= 0.5).astype(np.float)
            # print('pred_labels:', pred_labels)
            labels = data[2].cpu().numpy()
            # print('labels:', labels)
            acc = np.mean(pred_labels == labels)
            # print('acc:', acc)
            metrics.update(loss['bce_loss'].item())
            epoch_acc.update(acc)
            if self.clock.step % self.validation_step_freq == 0:
                data = next(test_loader_step)
                outputs, losses = self.step_test(data)
            self.clock.tick()
            if self.warm_restarts:
                self.scheduler.step(epoch + batch/iters)
        with self.train_tb:
            self.train_tb.add_scalar("epoch_loss", metrics.avg, global_step=self.clock.epoch)
            self.train_tb.add_scalar("epoch_acc", epoch_acc.avg, global_step=self.clock.epoch)
        
    
    def evaluate_batch(self, dataloader):
        metrics = AverageMeter("loss")
        epoch_acc = AverageMeter("acc")
        pbar = tqdm.tqdm(dataloader)
        self.model.eval()
        with torch.no_grad():
            for batch, data in enumerate(pbar):
                pbar.set_description(f"EVALUATION[{batch}]")
                outputs, loss = self.step_test(data, record=False)
                metrics.update(loss['bce_loss'].item())
                pred_labels = (torch.sigmoid(outputs).detach().cpu().numpy() >= 0.5).astype(np.float)
                # print('pred_labels:', pred_labels)
                labels = data[2].cpu().numpy()
                # print('labels:', labels)
                acc = np.mean(pred_labels == labels)
                # print('acc:', acc)
                epoch_acc.update(acc)

        with self.val_tb:
            self.val_tb.add_scalar("epoch_loss", metrics.avg, global_step=self.clock.epoch)
            self.val_tb.add_scalar("epoch_acc", epoch_acc.avg, global_step=self.clock.epoch)
        
        return epoch_acc.avg
        
        
class DenoiserWorker(BaseWorker):
    def __init__(self,config,device=None):
        self.validation_step_freq = config['validation_step_freq']
        super().__init__(config,device)

    def add_graph(self,):
        with self.train_tb:
            self.train_tb.add_graph(self.model_attr_accessor,
            (to_device(torch.randn((1,1,257,201)),device=self.device), to_device(torch.randn((1,1,32000)),device=self.device)))

    def model_build(self):
        """Build model for Denoiser"""
        model = FullDenoiserModel()
        if self.device != torch.device('cpu'):
           model = to_parallel(model)
        model = to_device(model,self.device)

        self.helper = HelperDenoiserSpec()
        #if self.device != torch.device('cpu'):
        #    self.helper = to_parallel(self.helper)
        self.helper = to_device(self.helper,self.device)
        return model 
    
    def set_loss_function(self):
        """set loss function used in training for denoiser"""
        return to_device(SSIM(win_size=11, win_sigma=1.5, data_range=1, size_average=True, channel=1),self.device)

    def set_optimizer(self, model):
        """set optimizer and lr scheduler used in training of denoiser"""
        return optim.SGD(model.parameters(), lr=self.lr, momentum=0.9, weight_decay=1e-6 )

    def set_scheduler(self, opt, steps=2, mult=2):
        """set lr scheduler used in training of denoiser"""
        if self.warm_restarts:
            scheduler = optim.lr_scheduler.CosineAnnealingWarmRestarts(opt,
                                                                T_0=steps,
                                                                T_mult=mult,
                                                                verbose=True)
        else:
            scheduler = optim.lr_scheduler.StepLR(opt, self.lr_step_size)
        return scheduler

    def step_train(self, data, record = True):
        """step for training"""
        # Batch order in DeviceDataLoader
        # data[0] audio
        # data[1] noise
        # data[2] noise_profile
        # data[3] noisy audio

        audio = data[0]
        noise = data[1]
        noise_profile = data[2]
        noisy_audio = data[3]

        self.helper.eval()
        with torch.no_grad():
            spec_audio, spec_noise, spec_noisy_audio = self.helper(audio,noise,noisy_audio)

        input_data = [spec_audio, spec_noise, spec_noisy_audio, noise_profile]
        self.model.train()
        output, loss = self.forward(input_data)
        self.update_network(loss)
        if record:
            self.record_losses(loss,'TRAIN')
        return output, loss
    
    def step_test(self, data, record = True):
        """step for training"""
        audio = data[0]
        noise = data[1]
        noise_profile = data[2]
        noisy_audio = data[3]

        self.helper.eval()
        with torch.no_grad():
            spec_audio, spec_noise, spec_noisy_audio = self.helper(audio,noise,noisy_audio)

        input_data = [spec_audio, spec_noise, spec_noisy_audio, noise_profile]

        self.model.eval()
        with torch.no_grad():
            output, loss = self.forward(input_data)
        if record:
            self.record_losses(loss,'VAL')
        return output, loss

    def forward(self, data):
        
        spec_audio = data[0]
        spec_noise = data[1]
        spec_noisy_audio = data[2]
        noise_profile = data[3]

        noise_pred, audio_mask_pred, audio_recovered = self.model(spec_noisy_audio, noise_profile)
        #recover_stft = complex_element_wise_w_logit(spec_noisy_audio, audio_mask_pred)

        loss_noise_profiler = 1 - self.criterion(noise_pred, spec_noise)
        loss_audio_pred = 1 - self.criterion(audio_recovered, spec_audio)
        return (noise_pred, audio_mask_pred) ,{"loss_noise_profiler": loss_noise_profiler, "loss_audio_pred": loss_audio_pred}
    
    def train_batch(self,train_loader, test_loader_step, epoch):
        epoch_loss_audio = AverageMeter("loss_audio")
        epoch_loss_noise = AverageMeter("loss_noise")
        iters = len(train_loader)
        pbar = tqdm.tqdm(train_loader)
        record = True
        for batch, data in enumerate(pbar):
            output, loss = self.step_train(data, record=record)
            pbar.set_description(f"EPOCH[{self.clock.epoch}][{batch}] LR={self.scheduler.get_last_lr()}")
            pbar.set_postfix(OrderedDict({k: v.item() for k, v in loss.items()}))
            epoch_loss_audio.update(loss["loss_audio_pred"].item())
            epoch_loss_noise.update(loss["loss_noise_profiler"].item())
            
            if self.clock.step % self.validation_step_freq == 0:
                data = next(test_loader_step)
                outputs, losses = self.step_test(data, record=record)

            record = False
            if self.clock.step % self.validation_step_freq*2 == 0:
                record = True
                
            self.clock.tick()
            if self.warm_restarts:
                self.scheduler.step(epoch + batch/iters)
        with self.train_tb:
            self.train_tb.add_scalar("epoch_loss_audio", epoch_loss_audio.avg, global_step=self.clock.epoch)
            self.train_tb.add_scalar("epoch_loss_noise", epoch_loss_noise.avg, global_step=self.clock.epoch)
        
    
    def evaluate_batch(self, dataloader):
        epoch_loss_audio = AverageMeter("loss_audio")
        epoch_loss_noise = AverageMeter("loss_noise")
        pbar = tqdm.tqdm(dataloader)
        self.model.eval()
        for batch, data in enumerate(pbar):
            pbar.set_description(f"EVALUATION[{batch}]")
            outputs, loss = self.step_test(data, record=False)
            epoch_loss_audio.update(loss["loss_audio_pred"].item())
            epoch_loss_noise.update(loss["loss_noise_profiler"].item())

        with self.val_tb:
            self.val_tb.add_scalar("epoch_loss_audio", epoch_loss_audio.avg, global_step=self.clock.epoch)
            self.val_tb.add_scalar("epoch_loss_noise", epoch_loss_noise.avg, global_step=self.clock.epoch)
        
        return epoch_loss_audio.avg
        
        