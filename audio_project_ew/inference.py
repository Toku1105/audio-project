import numpy as np 
import librosa 
import torch 
import os
from .model import VoiceActivityDetector, FullDenoiserModel
from .torch_utils import to_parallel, to_device, get_default_device, to_tensor, element_wise_w_logit
import logging
logger = logging.getLogger('develop')

class InferVAD():
    """ Helper to perform inference with VAD model
    Args:
        model_path(str): Path to VAD model 
        sr(int): Input audios sr, 16000 default and required, resample will be performed otherwise
        windows_duration(float): window duration of VAD segment to process
        threshold(float): [0-1] probability (SIGMOID) to consider as speech or not speech 
        mode(str): 'cpu' or 'gpu' to perform inference
        vad_model(torch.model): pre built model
    """
    def __init__(self, model_path:str,
                       sr:int=16000,
                       window_duration:float=0.020,
                       threshold:float=0.5,
                       mode:str='gpu',
                       vad_model=None): 
        self.path_vad_model = model_path 
        self.mode = mode
        if sr !=  16000:
            logger.warning('SR recommended 16000, all entries will be resampled')
        self.sr = sr
        self.window_duration = window_duration
        self.threshold = threshold
        self.vad_model = vad_model or self.load_deep_vad_model(self.path_vad_model)
        self.vad_model.eval()

    def audio_from_VAD(self, audio:np.ndarray,): 
        """ Perform inference in model and extract sements"""
        audio = self.verify_input(audio)
        prob_segments, spec = self.apply_vad(audio)
        segments = map(self.segments_to_dict, prob_segments)
        return list(segments), prob_segments, spec

    def verify_input(self, audio:np.ndarray):
        """ Verify is a single channel audio or batch of single channel audios"""
        if len(audio.shape)>2:
            raise Exception('Input should be 1D or 2D for batches of audio')
        return np.expand_dims(audio, axis=0) if len(audio.shape)==1 else audio

    def apply_vad(self, audio:np.ndarray,)->list:

        ''' Our DeepVAD
            Args:
                audio(np.array): audio only 1 channel or batch of audios
                windows_duration(float): duration of windows ms 
                threshold: from 0.0 to 1.0 probability selection
                mode: 'cpu' or 'gpu'
                sr: sampling rate to read, default(self sr instance)
            Returns:
                segments(list): return list of segments with boolean True if is speech otherwise False
        '''
        audio = np.asarray([librosa.resample(a, self.sr, 16000) for a in audio])

        v_num_frames = int(audio.shape[-1]/(16000*self.window_duration))
        audio = to_tensor(audio).unsqueeze(1)

        if self.mode =='gpu':    
            audio = to_device(audio,device = get_default_device())
        
        segments, spec = self.vad_model(x=audio, v_num_frames=v_num_frames)
        segments = torch.sigmoid(segments).cpu().detach().numpy()
        return segments, spec

    def load_deep_vad_model(self,load_path):
        """ Load model either GPU or CPU"""
        deep_vad_model = VoiceActivityDetector()
        if not os.path.exists(load_path):
            raise ValueError(f"Checkpoint {load_path} not exists.")
        if self.mode =='gpu':
            deep_vad_model.load_state_dict(torch.load(load_path)['model_state_dict'])
            deep_vad_model = to_device(deep_vad_model, device = get_default_device())
        else:
            deep_vad_model.load_state_dict(torch.load(load_path,map_location=torch.device('cpu'))['model_state_dict'])
        print(f'Model VAD Loaded on {self.mode} from {load_path}')
        return deep_vad_model

    def segments_to_dict(self,
                    segments:np.ndarray,):
        ''' Get segments from sigmoid output and convert array to webrtc output format 
            Args:
                segments(np.ndarray): probabilities of segment being speech or not
                sampling_rate(int): sampling rate default: 16000
                windows_duration(float): preferable 0.020
                threshold(float): [0-1] value to decide if segments is speech or not given probability 
            Returns:
                audio(np.ndarray): segmented array
        '''
        sampling_rate:int=16000
        step = int(sampling_rate * self.window_duration)
        return [{'start':idx*step, 
                'stop':idx*step+step, 
                'is_speech':seg>self.threshold} for idx,seg in enumerate(segments)]

class InferDenoiser(InferVAD):
    """ Helper to perform inference with Denoiser model
    Args:
        path_vad_path(str): Path to VAD model 
        path_denoiser_path(str): Path to DENOISER model
        mode(str): 'cpu' or 'gpu' to perform inference
        vad_model(torch.model): pre built vad model
        denoiser_model(torch.model): pre built model
    """
    def __init__(self, path_vad_model:str, 
                path_denoiser_model:str, 
                mode:str='gpu',
                vad_model=None, 
                denoiser_model=None):
        super().__init__(path_vad_model,mode=mode, vad_model=vad_model)
        self.path_denoiser_model = path_denoiser_model
        self.denoiser_model = denoiser_model or self.load_deep_denoiser_model(self.path_denoiser_model)
        self.denoiser_model.eval()

    def load_deep_denoiser_model(self,load_path):
        """ Load model either in GPU or CPU """
        deep_denoiser_model = FullDenoiserModel()
        if not os.path.exists(load_path):
            raise ValueError(f"Checkpoint {load_path} not exists.")
        print(self.mode)
        if self.mode =='gpu':
            deep_denoiser_model.load_state_dict(torch.load(load_path)['model_state_dict'])
            deep_denoiser_model = to_device(deep_denoiser_model, device = get_default_device())
        else:
            deep_denoiser_model.load_state_dict(torch.load(load_path,map_location=torch.device('cpu'))['model_state_dict'])
        print(f'Model DENOISER Loaded on {self.mode} from {load_path}')
        return deep_denoiser_model

    def runDenoiser(self,audio:np.ndarray):
        """ Perform inference in VAD and denoiser model consecutively"""
        audio = self.verify_input(audio)
        seg,segments,noisy_spec = self.audio_from_VAD(audio)
        print(audio.shape)
        noise_profiles = map(self.get_noise_profile, audio, segments)
        output = self.apply_denoiser(noisy_spec, list(noise_profiles))
        output.update({'segments':seg})
        return output

    def apply_denoiser(self, noisy_spec, noise_profiles):
        """ Perform single inference in denoiser"""
        noise_profiles = to_tensor(noise_profiles).unsqueeze(1)
        print(noise_profiles.shape)
        if self.mode =='gpu':    
            noisy_spec = to_device(noisy_spec, device = get_default_device())
            noise_profiles = to_device(noise_profiles , device = get_default_device())
        
        noise_pred, audio_mask_pred, audio_recovered = self.denoiser_model(noisy_spec, noise_profiles)
        #recover_stft = element_wise_w_logit(noisy_spec, audio_mask_pred, norm=False)
        return {'recovered_audio_spec':audio_recovered.cpu().detach(), 'noise_pred_spec':noise_pred.cpu().detach()}

    def get_noise_profile(self, 
                        audio:np.ndarray, 
                        segments:np.ndarray, 
                        sr:int=16000,)->np.ndarray: 

        """ Mutiply input audio with segments 0 if speech or 1 non speech to extract noise only
        Args:
            audio(np.ndarray): original audio
            segments(list): list of probabilities for each segment being speech or non speech
            sr(int) sampling rate 16000
        return:
            audio(np.ndarray): noise from audio with original shape
        """

        audio = audio.squeeze()
        tmp_mask = np.zeros_like(audio)
        for i, bit in enumerate(segments): 
            tmp_mask[int(i* sr* self.window_duration): int(i * sr * self.window_duration + sr*self.window_duration)] = int(bit>self.threshold)
        audio = audio * (1-tmp_mask)
        return audio 

if __name__ == "__main__":
    name = 'best_acc'
    vad_path = os.path.join('./models/VAD', f"{name}.pth")
    deep_vad = InferVAD(vad_path, mode='gpu')
    audios = np.random.rand(5,64000)
    seg,prob_seg,spec = deep_vad.audio_from_VAD(audios)
    print(len(seg), len(seg[0]))

    name = 'best_loss'
    denoiser_path = os.path.join('./models/DENOISER', f"{name}.pth")
    deep_denoiser = InferDenoiser(path_vad_model=vad_path, path_denoiser_model=denoiser_path, mode = 'gpu')
    output = deep_denoiser.runDenoiser(audios)
    for keys, data in output.items():
        print(keys, len(data), len(data[0]))