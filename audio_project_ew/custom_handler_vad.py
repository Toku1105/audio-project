from ts.torch_handler.base_handler import BaseHandler
from abc import ABC
import io
import base64
import torch
import numpy as np
from captum.attr import IntegratedGradients
import os
import importlib.util
import inspect
import json
import gzip
import logging
logger = logging.getLogger(__name__)
os.environ["CUDA_VISIBLE_DEVICES"] = '0'


class CustomHandler(BaseHandler, ABC):
    """
    Base class for handlers
    """


    def initialize(self, context):
        print(context.system_properties)
        super().initialize(context)
        self.v_num_frames = None
        self.ig = IntegratedGradients(self.model)
        self.initialized = True

    def _load_pickled_model(self, model_dir, model_file, model_pt_path):
        """
        Loads the pickle file from the given model path.
        Args:
            model_dir (str): Points to the location of the model artefacts.
            model_file (.py): the file which contains the model class.
            model_pt_path (str): points to the location of the model pickle file.
        Raises:
            RuntimeError: It raises this error when the model.py file is missing.
            ValueError: Raises value error when there is more than one class in the label,
                        since the mapping supports only one label per class.
        Returns:
            serialized model file: Returns the pickled pytorch model file
        """
        model_def_path = os.path.join(model_dir, model_file)
        if not os.path.isfile(model_def_path):
            raise RuntimeError("Missing the model.py file")

        module = importlib.import_module(model_file.split(".")[0])
        classes = [cls[1] for cls in inspect.getmembers(module, lambda member: inspect.isclass(member) and member.__module__ == module.__name__)]
        
        model_class = None
        for idx, cl in enumerate(classes):
            if cl.__name__=='VoiceActivityDetector':
                logger.info('VAD classes done')
                model_class = classes[idx]

        if model_class is None:
            raise RuntimeError("Model Class not loaded")

        model = model_class()
        if model_pt_path:
            logger.info(f'VAD STATE_DIC to {self.device}')
            state_dict = torch.load(model_pt_path, map_location=self.device)
            logger.info('VAD state_dic loaded')
            model.load_state_dict(state_dict['model_state_dict'])
            logger.info('VAD model loaded')
        return model
        
    def preprocess(self, data):
        """The preprocess function of MNIST program converts the input data to a float tensor
        Args:
            data (List): Input data from the request is in the form of a Tensor
        Returns:
            list : The preprocess function returns the input image as a list of float tensors.
        """
        # Compat layer: normally the envelope should just return the data
        # directly, but older versions of Torchserve didn't have envelope.
        audio = data[0].get("data") or data[0].get("body")
        audio = np.asarray(audio)
        audio = self._audio_processing(audio)
        

        return audio.to(self.device)

    def inference(self, data):

        if self.v_num_frames is None: 
            raise RuntimeError('num of frames for vad invalid')

        with torch.no_grad():
            segments, spec = self.model(x=data, v_num_frames=self.v_num_frames)
        
        segments = torch.sigmoid(segments)
        return (data,segments, spec)

    def postprocess(self, inference_output):
        data, prob_segments, spec = inference_output
        data = data.cpu().detach().numpy()
        prob_segments =  prob_segments.cpu().detach().numpy()
        spec = spec.cpu().detach().numpy()

        segments = map(self.segments_to_dict, prob_segments)
        noise_profiles = list(map(self.get_noise_profile, data, prob_segments))
        output = {'segments':list(segments), 'noise_profiles':[l.tolist() for l in noise_profiles], 'specs':spec.tolist()}
        return [gzip.compress(json.dumps(output).encode('utf-8'))]#self.dict_to_json(output)

    def dict_to_json(self,dic:dict):
        for key, val in dic.items(): 
            if isinstance(val, np.ndarray):
                memfile = io.BytesIO()
                np.save(memfile, val)
                memfile.seek(0)
                encoded = memfile.read().decode('latin-1')
                dic[key] = encoded
        return json.dumps(dic)

    def segments_to_dict(self, segments:np.ndarray,):
        ''' Get segments from sigmoid output and convert array to webrtc output format 
            Args:
                segments(np.ndarray): probabilities of segment being speech or not
                sampling_rate(int): sampling rate default: 16000
                windows_duration(float): preferable 0.020
                threshold(float): [0-1] value to decide if segments is speech or not given probability 
            Returns:
                audio(np.ndarray): segmented array
        '''
        sampling_rate:int=16000
        step = int(sampling_rate * 0.020)
        
        return [{'start':idx*step, 
            'stop':idx*step+step, 
            'is_speech': 'true' if seg>0.5 else 'false'} for idx, seg in enumerate(segments)]
    
    def get_noise_profile(self, 
                        audio:np.ndarray, 
                        segments:np.ndarray, 
                        sr:int=16000,)->np.ndarray: 

        """ Mutiply input audio with segments 0 if speech or 1 non speech to extract noise only
        Args:
            audio(np.ndarray): original audio
            segments(list): list of probabilities for each segment being speech or non speech
            sr(int) sampling rate 16000
        return:
            audio(np.ndarray): noise from audio with original shape
        """

        audio = audio.squeeze()
        tmp_mask = np.zeros_like(audio)
        for i, bit in enumerate(segments): 
            tmp_mask[int(i* sr*0.020): int(i * sr * 0.020 + sr*0.020)] = int(bit>0.5)
        audio = audio * (1-tmp_mask)
        return audio

    def _audio_processing(self, audio:np.ndarray)->np.ndarray:
        audio = self._verify_input(audio)
        self.v_num_frames = int(audio.shape[-1]/(16000*0.020))
        audio = torch.FloatTensor(audio).unsqueeze(1)
        return audio 

    
    def _verify_input(self, audio:np.ndarray)->np.ndarray:
        """ Verify is a single channel audio or batch of single channel audios"""
        if len(audio.shape)>2:
            raise Exception('Input should be 1D or 2D for batches of audio')
        return np.expand_dims(audio, axis=0) if len(audio.shape)==1 else audio

