import torch
import torch.nn as nn 
import torch.nn.functional as F
import torchaudio.transforms as T


class VoiceActivityDetector(nn.Module):
    def __init__(self,):
        super().__init__()
        self.spec = Spectrogram(n_fft=512,
                                win_len=400,
                                hop_len=160,
                                power=2,
                                mean = 0.,
                                std = 1.)
        self.vad = VADNet(freq_bins=512//2+1)
        
    def forward(self, x, v_num_frames=100):
        spec, _ = self.spec(x)
        x = self.vad(spec, v_num_frames)
        return x, spec

class FullDenoiserModel(nn.Module):
    def __init__(self):
        super().__init__()
        class Config():
            kernel_sizes = [(1, 7), (7, 1), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5)]
            dilations    = [(1, 1), (1, 1), (1, 1), (2, 1), (4, 1), (8, 1), (16, 1), (32, 1), (1, 1), (2, 2), (4, 4), (8, 8), (16, 16), (32, 32)]
        config = Config()
        self.spec_noise_profile = Spectrogram(n_fft=512,
                                win_len=400,
                                hop_len=160,
                                power=2,
                                mean = 0.,
                                std = 1.)
        self.noise_profile = NoiseProfileModel()
        self.audio_predict = DenoiserModel(config.kernel_sizes, config.dilations)

    def forward(self, noisy_audio_spec, noise_profile):
        noise_profile_spec, _ = self.spec_noise_profile(noise_profile)
        noise_pred = self.noise_profile(noise_profile_spec, noisy_audio_spec)
        audio_mask_pred = self.audio_predict(noisy_audio_spec, noise_pred)
        audio_recovered = noisy_audio_spec * audio_mask_pred 
        return noise_pred, audio_mask_pred, audio_recovered

class HelperDenoiserSpec(nn.Module):
    def __init__(self):
        super().__init__()
        mean = 0.
        std = 1.
        self.spec_noise = Spectrogram(n_fft=512,win_len=400,hop_len=160,power=2,mean = mean,
                                std = std)
        self.spec_noisy_audio  = Spectrogram(n_fft=512,win_len=400,hop_len=160,power=2,mean = mean,
                                std = std)
        self.spec_audio = Spectrogram(n_fft=512,win_len=400,hop_len=160,power=2,mean = mean,
                                std = std)
    def forward(self, audio, noise, noisy_audio):
        return (self.spec_audio(audio)[0], self.spec_noise(noise)[0] ,self.spec_noisy_audio(noisy_audio)[0])

class Spectrogram(nn.Module):
    def __init__(self,
                 n_fft=400, 
                 win_len=None, 
                 hop_len=None,
                 power=None,
                 mean = 0.,
                 std = 1.
    ):
        super().__init__()
        self.power = power
        self.mean = mean
        self.std = std
        self.spectrogram = T.Spectrogram(
            n_fft=n_fft,
            win_length=win_len,
            hop_length=hop_len,
            center=True,
            pad_mode="reflect",
            power=power,
        )
    def forward(self, x):
        spec = self.spectrogram(x)
        if self.power is None:
            spec = spec.squeeze(1).permute((0,3,1,2)) ## Not support for two channels audio 
        shape = spec.size()
        spec = spec.view(spec.size(0), -1)
        mini = spec.min(1, keepdim=True)[0]
        
        maxi = spec.max(1, keepdim=True)[0]  + 1e-11
        spec = (spec - mini)/(maxi)
        spec = spec.view(shape)
        return spec,(mini,maxi)

class VADNet(nn.Module):
    def __init__(self,
                 freq_bins=257,
                 #time_bins=201,
                 conv_filters=48):
        super().__init__()

        self.n_layers = 1
        self.hidden_size = 100
        audio_kernel_sizes = [(1, 7), (7, 1), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5)]
        audio_dilations    = [(1, 1), (1, 1), (1, 1), (2, 1), (4, 1), (8, 1), (1, 1), (2, 2), (4, 4), (8, 8)]
        self.encoder_audio = self.make_audio_branch(audio_kernel_sizes, audio_dilations, filters=conv_filters, outf=8)

        self.lstm = nn.LSTM(input_size=8*freq_bins, hidden_size=self.hidden_size, num_layers=self.n_layers, bidirectional=True)
        self.fc1 = nn.Sequential(nn.Linear(200, 100),
                                nn.GELU(),
                                nn.Dropout(p=0.4),
                                nn.Linear(100, 1))

    def make_audio_branch(self, kernel_sizes, dilations, filters=96, outf=8):
        encoder_x = []
        for i in range(len(kernel_sizes)):
            if i == 0:
                encoder_x.append(_Conv2dBlock(1, filters, kernel_sizes[i], dilations[i]))
            else:
                encoder_x.append(_Conv2dBlock(filters, filters, kernel_sizes[i], dilations[i]))
        encoder_x.append(_Conv2dBlock(filters, outf, (1, 1), (1, 1)))
        encoder_x.append(nn.Dropout(p=0.0))
        return nn.Sequential(*encoder_x)

    def forward(self, s, v_num_frames=100):
        f_s = self.encoder_audio(s)
        f_s = f_s.view(f_s.size(0), -1, f_s.size(3)) # (B, C1, T1)
        f_s = F.interpolate(f_s, size=v_num_frames) # (B, C2, T1)
        merge = f_s
        merge = merge.permute(2, 0, 1)  # (T1, B, C1+C2)
        self.lstm.flatten_parameters()
        merge, hidden = self.lstm(merge)
        merge = merge.permute(1, 0, 2)# (B, T1, C1+C2)
        merge = self.fc1(merge)
        out = merge.squeeze(2)

        return out



class DenoiserModel(nn.Module):
    def __init__(self,
                 kernel_sizes,
                 dilations,
                 freq_bins=257,
                 nf=96):
        super().__init__()
        self.encoder_x = self.make_enc(kernel_sizes, dilations, nf)
        self.encoder_n = self.make_enc(kernel_sizes, dilations, nf // 2, outf=4)

        self.lstm = nn.LSTM(input_size=8*freq_bins + 4*freq_bins, hidden_size=100, bidirectional=True)
        self.fc = nn.Sequential(nn.Linear(200, 512),
                                nn.GELU(),
                                nn.Dropout(p=0.2),
                                nn.Linear(512, 512),
                                nn.GELU(),
                                nn.Dropout(p=0.2),
                                nn.Linear(512, freq_bins),
                                nn.Sigmoid())

    def make_enc(self, kernel_sizes, dilations, nf=96, outf=8):
        encoder_x = []
        for i in range(len(kernel_sizes)):
            if i == 0:
                encoder_x.append(_Conv2dBlock(1, nf, kernel_sizes[i], dilations[i],act='gelu'))
            else:
                encoder_x.append(_Conv2dBlock(nf, nf, kernel_sizes[i], dilations[i],act='gelu'))
        encoder_x.append(_Conv2dBlock(nf, outf, (1, 1), (1, 1),act='gelu'))
        encoder_x.append(nn.Dropout(0.2))
        return nn.Sequential(*encoder_x)

    def forward(self, x, n):
        f_x = self.encoder_x(x)
        f_x = f_x.view(f_x.size(0), -1, f_x.size(3)).permute(2, 0, 1)
        f_n = self.encoder_n(n)
        f_n = f_n.view(f_n.size(0), -1, f_n.size(3)).permute(2, 0, 1)
        self.lstm.flatten_parameters()
        f_x, _ = self.lstm(torch.cat([f_x, f_n], dim=2))
        f_x = f_x.permute(1, 0, 2)
        f_x = self.fc(f_x)
        out = f_x.permute(0, 2, 1).view(f_x.size(0), 1, -1, f_x.size(1))
        # f_n = self.encoder_n(n)
        return out

class _Conv2dBlock(nn.Module):
    def __init__(self, 
                 in_channels, 
                 out_channels, 
                 kernel_size:tuple, 
                 dilation:tuple,
                 stride=1,
                 norm_fn='bn',
                 act='gelu'
    ):
        super().__init__()
        pad = ((kernel_size[0] - 1) // 2 * dilation[0], (kernel_size[1] - 1) // 2 * dilation[1])
        block = []
        block.append(nn.Conv2d(in_channels, 
                               out_channels, 
                               kernel_size, 
                               stride, pad, 
                               dilation, 
                               bias=norm_fn is None))
        if norm_fn == 'bn':
            block.append(nn.BatchNorm2d(out_channels))
        if act == 'relu':
            block.append(nn.ReLU())
        elif act == 'prelu':
            block.append(nn.PReLU())
        elif act == 'lrelu':
            block.append(nn.LeakyReLU())
        elif act == 'tanh':
            block.append(nn.Tanh())
        elif act == 'gelu':
            block.append(nn.GELU())
        block.append(nn.Dropout(0.1))

        self.block = nn.Sequential(*block)

    def forward(self, x):
        return self.block(x)


class UpConvBlock(nn.Module):
    def __init__(self, 
                 in_channels, 
                 out_channels, 
                 kernel_size, 
                 stride,
                 dilation=1,
                 norm_fn='bn',
                 act='gelu',
                 up_mode='upconv'):
        super().__init__()
        pad = (kernel_size - 1) // 2 * dilation
        block = []
        if up_mode == 'upconv':
            block.append(nn.ConvTranspose2d(in_channels, out_channels, kernel_size, stride, pad, dilation,
                                            bias=norm_fn is None))
        elif up_mode == 'upsample':
            block.append(nn.Upsample(scale_factor=2))
            block.append(nn.ReflectionPad2d(pad))
            block.append(nn.Conv1d(in_channels, out_channels, kernel_size, stride, 0, dilation,
                                   bias=norm_fn is None))
        if norm_fn == 'bn':
            block.append(nn.BatchNorm2d(out_channels))
        if act == 'prelu':
            block.append(nn.PReLU())
        elif act == 'lrelu':
            block.append(nn.LeakyReLU())
        elif act == 'gelu':
            block.append(nn.GELU())
        elif act == 'tanh':
            block.append(nn.Tanh())

        self.block = nn.Sequential(*block)

    def forward(self, x):
        return self.block(x)

class DownConvBlock(nn.Module):
    def __init__(self, 
                 in_channels, 
                 out_channels, 
                 kernel_size, 
                 stride,
                 dilation=1,
                 norm_fn='bn',
                 act='gelu'):
        super().__init__()
        pad = (kernel_size - 1) // 2 * dilation
        block = []
        block.append(nn.ReflectionPad2d(pad))
        block.append(nn.Conv2d(in_channels, out_channels, kernel_size, stride, 0, dilation, bias=norm_fn is None))
        if norm_fn == 'bn':
            block.append(nn.BatchNorm2d(out_channels))
        if act == 'prelu':
            block.append(nn.PReLU())
        elif act == 'gelu':
            block.append(nn.GELU())
        elif act == 'lrelu':
            block.append(nn.LeakyReLU())
        elif act == 'sigmoid':
            block.append(nn.Sigmoid())

        self.block = nn.Sequential(*block)

    def forward(self, x):
        return self.block(x)

class NoiseProfileModel(nn.Module):
    def __init__(self):
        super().__init__()
        ch1 = 16
        ch2 = 64
        ch3 = 128

        self.down1 = nn.Sequential(
            DownConvBlock(1, ch1, 5, 1),
            DownConvBlock(ch1, ch1, 5, 1),
        )
        self.down2 = nn.Sequential(
            DownConvBlock(ch1, ch1*2, 5, 2),
            DownConvBlock(ch1*2, ch2, 5, 1),
            DownConvBlock(ch2, ch2, 5, 1),
        )
        self.down3 = nn.Sequential(
            DownConvBlock(1, ch1, 5, 1),
        )
        self.down4 = nn.Sequential(
            DownConvBlock(ch1, ch1*2, 5, 2),
            DownConvBlock(ch1*2, ch2, 5, 1),
            DownConvBlock(ch2, ch2, 5, 1),
        )
        self.mid = nn.Sequential(
            DownConvBlock(ch2 * 2, ch3, 3, 2),
            DownConvBlock(ch3, ch3, 3, 1),
            DownConvBlock(ch3, ch3, 3, 1, dilation=2),
            DownConvBlock(ch3, ch3, 3, 1, dilation=4),
            DownConvBlock(ch3, ch3, 3, 1, dilation=8),
            DownConvBlock(ch3, ch3, 3, 1, dilation=16),
            DownConvBlock(ch3, ch3, 3, 1),
            DownConvBlock(ch3, ch2, 3, 1),
            UpConvBlock(ch2, ch2, 3, 2),
        )
        self.up1 = nn.Sequential(
            DownConvBlock(ch2 * 2, ch2, 3, 1),
            UpConvBlock(ch2, ch1, 3, 2),
        )
        self.up2 = nn.Sequential(
            DownConvBlock(ch1 * 2, ch1, 3, 1),
            DownConvBlock(ch1, 1, 3, 1, norm_fn=None, act='sigmoid')
        )

    def forward(self, x, y):
        down1 = self.down1(x)
        down2 = self.down2(down1)

        down3 = self.down3(y)
        down4 = self.down4(down3)
        out = self.mid(torch.cat([down2, down4], dim=1))
        if out.shape != down4.shape:
            out = F.interpolate(out, down4.size()[-2:])
        out = self.up1(torch.cat([out, down4], dim=1))
        if out.shape != down3.shape:
            out = F.interpolate(out, down3.size()[-2:])
        out = self.up2(torch.cat([out, down3], dim=1))
        return out


if __name__ == '__main__':
    from torchinfo import summary
    from torch_utils import to_device, get_default_device
    device = get_default_device()
    class Config():
        kernel_sizes = [(1, 7), (7, 1), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5), (5, 5)]
        dilations    = [(1, 1), (1, 1), (1, 1), (2, 1), (4, 1), (8, 1), (16, 1), (32, 1), (1, 1), (2, 2), (4, 4), (8, 8), (16, 16), (32, 32)]
    config = Config()

    print('SPECTOGRAM MODEL')
    model = to_device(Spectrogram(n_fft=512,win_len=400,hop_len=160,power=2), device=device)
    summary(model, (1,1,32000))
    s = to_device(torch.randn((1,1,32000)), device=device)
    out,_ = model(s)
    print(out.shape)

    print('VAD MODEL')
    model = to_device(VADNet(), device=device) 
    summary(model, (1,1,257,201))
    s = to_device(torch.randn((1, 1, 257, 201)), device=device)
    out = model(s)
    print('output: ', out.shape)
    
    print('VOICE ACTIVITY DETECT (SPEC+VAD)')
    model = to_device(VoiceActivityDetector(), device=device)
    summary(model, (1,1,32000))
    s = to_device(torch.randn((1,1,32000)), device=device)
    out,spec = model(s)
    print(out.shape)
    print(spec.shape)

    print('Denoiser MODEL')
    model = to_device(DenoiserModel(config.kernel_sizes, config.dilations), device=device)
    summary(model, [(1,1,257,201),(1,1,257,201)])
    a = to_device(torch.randn((1, 1, 257, 201)), device=device)
    b = to_device(torch.randn((1, 1, 257, 201)), device=device)
    out = model(a,b)
    print('output: ', out.shape)

    print('NOISE PROFILE MODEL')
    model = to_device(NoiseProfileModel(), device=device) 
    summary(model, [(8,1,257,201),(8,1,257,201)])
    a = to_device(torch.randn((1, 1, 257, 201)), device=device)
    b = to_device(torch.randn((1, 1, 257, 201)), device=device)
    out = model(a,b)
    print('output: ', out.shape)

    print('FULL DENOISER MODEL')
    model = to_device(FullDenoiserModel(), device = device)
    summary(model, [(8,1,257,201),(8,1,32000)])
    a = to_device(torch.randn((2, 1, 257, 201)), device=device)
    b = to_device(torch.randn((2,1,32000)), device=device)
    out1,out2,out3 = model(a,b)
    print('output: ', out1.shape, out2.shape, out3.shape)