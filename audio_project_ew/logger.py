import os

LOGGING_CONFIG = {
    "version": 1,  # required
    "disable_existing_loggers": False,  # if True this config overrides all other loggers
    "formatters": {
        "simple": {"format": "%(asctime)s %(levelname)s -- %(message)s"},
        "detailed": {
            "format": "%(asctime)s\t%(levelname)s -- %(process)d -%(thread)d %(filename)s:%(lineno)s -- %(message)s"
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "detailed",
        },
        "file": {
            "level": "DEBUG",
            "filename": "./logs/log.log",
            "mode": "w",
            "class": "logging.FileHandler",
            "formatter": "detailed",
            "encoding": "utf-8",
        },
    },
    "loggers": {
        "nodebug":{ 
            "level":"NOTSET",
            "handlers":["console"],
                  },
        
        "develop": {
            "level":'DEBUG',
            "handlers": ["file"],
        },
        "fullDebug": {
            "level": "DEBUG", 
            "handlers": ["file"],
        },
    },
}

if not os.path.exists("./logs"):
    os.makedirs("./logs")

if __name__ == "__main__":

    import logging, logging.config

    logging.config.dictConfig(LOGGING_CONFIG)
    logger = logging.getLogger("fullDebug")
    logger.debug("debug message")
