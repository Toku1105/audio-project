import numpy as np
import torch 
import os


def makedirs(path):
    # Intended behavior: try to create the directory,
    # pass if the directory exists already, fails otherwise.
    # Meant for Python 2.7/3.n compatibility.
    try:
        os.makedirs(path)
    except OSError as e:
        if not os.path.isdir(path):
            raise

def normalize_audio(audio:np.ndarray):
    maxcutoff = audio.mean() + 4*audio.std()
    mincutoff = audio.mean() - 4*audio.std()
    a = np.clip(audio,mincutoff, maxcutoff)
    a = np.interp(a, (a.min(), a.max()), (-1, 1))
    a = np.divide(a,np.max(np.abs(a)), out=np.zeros_like(a, dtype=float),where=np.max(np.abs(a))!=0)
    return a 



def cut_or_pad(instance:np.ndarray,
               length:int, 
               stochastic:bool=True,
               mode='wrap')->np.ndarray:
    ''' Cut or pad an array given a target length
            Args:
                instance(np.ndarray): array to cut or pad
                length(int): target size
                stochastic(bool): cut or pad in random points. default:True 
                mode(str): mode for padding see np.pad docs. default:'wrap' 
            Returns:
                np.ndarray of instance with required length
        '''

    if length == -1:
        return instance
    less_timesteps = length - len(instance)
    
    if less_timesteps <= 0: #cut required len
        if stochastic:
            fragment_start_index = np.random.randint(
                0, max(abs(less_timesteps), 1)
            )
        else:
            fragment_start_index = 0

        instance = instance[fragment_start_index : fragment_start_index + length]
    
    else:
        
        if stochastic:
            before_len = np.random.randint(0, less_timesteps)
            after_len = less_timesteps - before_len
            instance = np.pad(instance, (before_len, after_len), mode)
        else:
            # Deterministic padding. Append audio to reach self.fragment_length
            instance = np.pad(instance, (0, less_timesteps), mode)
                    
    return instance
