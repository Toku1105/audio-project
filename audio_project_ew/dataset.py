""" Please Review tree structure for data folder, none dataset has been modified
however tables and csv files have been moved to data root, and be careful with 
tables from arab_iraq, since it has no columns initialized so please add the columns 
name from arab_gulf, NIST docs mov ed to root data, and audios moved to data... 
Folders with audios always in lowercase
ID Unique identifier of audio 
NAME: name for speaker or file with CHANNEL included could be repeated"""

import os
import gc
import glob
import torch
import numpy as np
import pandas as pd
import multiprocessing
from audio_project_ew.audio_processing import Audio
from audio_project_ew import utils 
from .dataset_generator import Generator
from .torch_utils import get_spectrogram, to_tensor
from concurrent.futures import ThreadPoolExecutor,ProcessPoolExecutor

from tqdm import tqdm, tqdm_notebook
from joblib import Parallel, delayed

import logging
logger = logging.getLogger('develop')

class SegmentGenerator(Generator):
    
    def __init__(
        self, config: dict
    ):
        """ Parameters:
                config: Arguments from dataset key in config json file
        """
        self.config = config
        super().__init__(**config)
        

        try:
            logger.info(f"Reading {self._datasets} datasets")
            df = self._read_datasets(self._datasets, drop_labels=["ID", "NAME", "CHANNEL"])
            self.sets = self._check_old_entries(set(df.SET.unique()))

            for setx in self.sets:
                logger.info(f"Preprocessing {setx} set... this can take a while...")
                self.audio_files = []
                self.audio_files = AudioGenerator.set_preprocessing(setx.lower(),
                                                          **config)
                self.audio_files = sum(
                    self.audio_files, []
                )  # memory inneficient but works
                logger.info(
                    f"Audio Files Founded: {len(self.audio_files)} for {setx} set"
                )
                logger.info(
                    f"Entries for {setx} set: {df.SET.value_counts()[setx]}"
                )

                if self.audio_files:
                    audio_df = pd.DataFrame(self.audio_files)
                    audio_df.ID = audio_df.ID.astype(str)
                    df.ID = df.ID.astype(str)
                    tmp_df = pd.merge(df, audio_df)
                    logger.info(f"Merged entries: {len(tmp_df)}")

                    if self._df is not None:
                        self._df = pd.concat([self._df, tmp_df], ignore_index=True)
                    else:
                        self._df = tmp_df

                    self._df.NAME = self._df.NAME.astype(str)
                    self._df.LANGUAGE = self._df.LANGUAGE.astype(str)
                    self._df.to_csv(os.path.join(config["annotations_path"],
                                                 config["output_metadata"]),
                                    index=False)
                else:
                    logger.warning(f"Set {setx} not found")

            self._df = self._df.reset_index(drop=True)

            if self.label not in self._df.keys():
                raise ValueError(f"Label must be in df keys: {self._df.keys()}")

            logger.info(
                f'Initialising AudioGenerator with length:{config["audio"]["output_secs"]}s and label:{self.label}'
            )

            if config["audio"]["noise"]:
                logger.info(f'Loading Noise Dataset Information')
                self._noise_df = self._read_datasets(config["noise_datasets"], drop_labels=None)
            else:
                logger.warning('No noise active, output could be incomplete')

        except Exception as e:
            try:
                self._df.to_csv(
                    os.path.join(config["data_path"], "metadata-error.csv"),
                    index=False,
                )
                del self._df
            except:
                logger.error("metadata-error could not be created")

            finally:
                gc.collect()
                raise e
        
        self._filter_dataset()
        self._update_properties()
        
        if self.phase=="TEST":
            logger.info('TEST DATASET')
            #self.config["audio"]["fragment_length"] = -1 # To return the audio 

    def _check_old_entries(self, sets: set) -> set:
        """Verify if sets exist in output dataframe
            Return sets of data missing"""
        
        output = os.path.join(self.config["annotations_path"], 
                              self.config["output_metadata"])

        if os.path.exists(output):
            logger.info(f"{output} file found ... extracting existing data")
            self._df = pd.read_csv(output)
            existing_sets = set(self._df.SET.unique())
            new_sets = sets - existing_sets
            logger.info(f"From {sets} sets, with existing sets: {existing_sets}... Reading new {new_sets} sets")
            
            return new_sets
        
        logger.info("No old output dataset found, creating new from scratch")
        
        return sets
    
    def _read_datasets(self, datasets: list, drop_labels: list) -> pd.DataFrame:
        """ 
        Extract dataframes given subsets 
        Parameters:
            datasets(list): list of names for dataset to read in data_path
            drop_labels(list): ids to compare and drop duplicates or None
        Return:
            pd.DataFrame: dataframe concatenated dropping all duplicates
        """
        dfs = []
        for subset in datasets:
            logger.info(
                f'Processing file: {os.path.join(self.config["data_path"],subset)}'
            )
            tmp = pd.read_csv(os.path.join(self.config["annotations_path"],
                                           subset)+'_annotations.csv')
            dfs.append(tmp)
            logger.info(f"Dataframe read :{subset}")
        df = pd.concat(dfs, ignore_index=True)
        
        return df.drop_duplicates(subset=drop_labels)

    def _filter_dataset(self):
        """
            Filter dataset to delete null spaces, unknown fields, audios length=0, phase and datasets
        """
        if self.phase == 'TEST':
            self._df = self._df[self._df.SUBSET.isin([self.phase,'VALIDATION'])]
        else:
            self._df = self._df[self._df.SUBSET.isin([self.phase])]
        self._df = self._df[self._df["SET"].isin(list(map(str.upper, self._datasets)))]
        self._df = self._df[~(self._df.LENGTH == 0)]
        self._df = self._df[
                ~(self._df[self.label].isna()) & ~(self._df[self.label] == "UNKNOWN")
            ]

        if len(self.classes_) > 0:
            self._df = self._df[self._df[self.label].isin(self.classes_)]

        self._df = self._df.reset_index(drop=True)

        if self.noise_:
            self._noise_df = self._noise_df[self._noise_df.SUBSET == self.phase]

    def _generate_triplets(self, idx: int) -> dict:
        """Generate random triplets"""
        positive = {}
        anchor = {}
        negative = {}

        positive_df = self._df.loc[idx]
        positive["category"] = positive_df[self.config["label"]]
        positive["path"] = positive_df["FILEPATH"]
        positive["channel"] = positive_df["CHANNEL"]

        anchor_df = self._df.loc[
            (self._df[self.config["label"]] == positive["category"])
            & (self._df.index != positive_df.name)
        ].sample(1)
        anchor["category"] = anchor_df[self.config["label"]].tolist()[0]
        anchor["path"] = anchor_df["FILEPATH"].tolist()[0]
        anchor["channel"] = anchor_df["CHANNEL"].tolist()[0]

        negative_df = self._df.loc[
            (self._df[self.config["label"]] != positive["category"])
        ].sample(1)
        negative["category"] = negative_df[self.config["label"]].tolist()[0]
        negative["path"] = negative_df["FILEPATH"].tolist()[0]
        negative["channel"] = negative_df["CHANNEL"].tolist()[0]

        assert (
            anchor["category"] == positive["category"]
            and positive["category"] != negative["category"]
        ), f"anchor {anchor}, positive{positive} and {negative}\
        must be differents"

        return {"anchor": anchor, "positive": positive, "negative": negative}

    def _generate_single(self, idx: int) -> dict:
        """Generate simple call"""

        single_df = self._df.loc[idx]
        single = {}
        single["category"] = self._label_to_category[single_df[self.config["label"]]]
        single["path"] = single_df["FILEPATH"]
        single["channel"] = single_df["CHANNEL"]

        if self.noise_:
            single["noise_path"] = self._noise_df.sample()['saved_path'].values


        return single

    def _get_voice_tensor(self, value: dict) -> dict:
        ap = Audio(sr=self.config['audio']['sampling_rate'])
        audio = ap.read(filepath=value["path"],
                            channel=value["channel"],
                            )
        audio = [utils.cut_or_pad(instance=single_channel, 
                    length=self.config["audio"]["fragment_length"],
                    stochastic=self.config["audio"]["stochastic"]) for single_channel in audio]
        
        if self.segments_:
            channel_segments = [ap.webrtc_vad(single_channel,window_duration=0.020) for single_channel in audio]
            segments = [[1 if segment['is_speech'] else 0 for segment in segments] for segments in channel_segments]
            value["segments"] = to_tensor(segments[0])

        if "noise_path" in value:
            # We support random choice but generate_single return a single sample, but it could be changed
            SNR = np.random.randint(-10,10)
            if np.random.choice([True,False], p=[0.85, 0.15]):
                noise = ap.load_noise(value["noise_path"])
                noise = [utils.cut_or_pad(instance=noise[0], length=self.config["audio"]["fragment_length"],
                    stochastic=True)]
                noisy_audio = [ap.mix_audios(single_channel, noise[0], SNR) for single_channel in audio]
            else:
                noise = [np.zeros_like(a) for a in audio]
                noisy_audio =  audio
            #get_spectrogram(to_tensor(noise),n_fft=512,win_len=400,hop_len=160,power=None).permute((0,3,1,2))
            value["noisy_audio"] = to_tensor(noisy_audio)
        
        value["audio"] = to_tensor(audio)
        #value["category"] =  torch.as_tensor(value["category"])
        return value


    @staticmethod
    def set_preprocessing(setx: str, 
                          data_path: str, 
                          **kwargs) -> list:

        # Get all folders
        if setx not in os.listdir(data_path):
            logger.info(f'{setx} not found in {data_path}')
            return []

        files = glob.iglob(os.path.join(data_path, 
                                        f"*{setx}*/**/*.*"), 
                           recursive=True)
        
        id_by_folder = False
        if len(list(filter(lambda x: os.path.isdir(x),
                           glob.glob(os.path.join(data_path, f"*{setx}*/**"))))) > 5:
            # If setx contains several folder usually each folder is a speaker, if its not 
            # speaker is part of the file name, we use that as an identifier in the dataframe
            # threshold in our case is 5 however it could be different for each case
            
            id_by_folder = True

        extensions = [".flac", ".mp3", ".aac", ".m4a", ".wav", ".sph"]
        
        all_files = []
        for file in files:
            
            if "split" in file:  # not to read splitted segments
                continue

            if os.path.splitext(file)[-1] in extensions:
                speech_id = (
                    file.split("/")[file.split("/").index("data") + 2]#../data(x)/dataset_name/id(x+2)/files.wav
                    if id_by_folder
                    else os.path.splitext(os.path.split(file)[-1])[-2]#[file,.ext][-2]
                )
                all_files.append({"file": file, "speech_id": speech_id})

        # Extract features
        all_files = tqdm_notebook(all_files)
        cpu_number = multiprocessing.cpu_count()
        jobs_num = (
            kwargs['audio']['preprocessing']['num_parallel']
            if kwargs['audio']['preprocessing']['num_parallel'] != -1 and kwargs['audio']['preprocessing']['num_parallel'] < cpu_number
            else cpu_number
        )
        logger.info(
            f"Starting to extract features from audio files:{len(all_files)}, runnning with {jobs_num} processes"
        )
        audio_files = Parallel(n_jobs=jobs_num)(
            delayed(AudioGenerator.extract_features)(audio, **kwargs["audio"]["preprocessing"])
            for audio in all_files
        )
        all_files.close()

        return audio_files

    @staticmethod
    def extract_features(audio: list, **kwargs) -> dict:
        speech_id = audio["speech_id"]
        full_file_path = audio["file"]
        ap = Audio(sr=kwargs['sampling_rate'], filepath=full_file_path)
        instances = ap.audio
        output = []
        for idx, audio in enumerate(instances):
            details = {
                "ID": str(speech_id).upper(),
                "FILEPATH": full_file_path,
                "LENGTH": len(audio),
                "SECONDS": len(audio) * 1.0 / kwargs["sampling_rate"],
                "CHANNEL": int(idx),
            }

            if kwargs["split_large_audios"] and details["SECONDS"] > kwargs["max_seconds_per_split"]:
                logger.info(f'Splitting audio {details["ID"]}')
                details = ap.split_audio(audio, 
                            ID=details['ID'],
                            channel=details['CHANNEL'],
                            seconds_per_split=kwargs["max_seconds_per_split"],
                            store_split=kwargs["store_split"])

            if not isinstance(details, list):
                details = [details]
            logger.info(details)
            output += details
        return output

                              
    def _update_properties(self):
        self.config["audio"]["fragment_length"] = int(
            self.config["audio"]["output_secs"] * self.config["audio"]["sampling_rate"]
        )
        self.config["audio"]["preprocessing"]["store_split"] = False

        LABEL = self.config["label"]

        self._label_to_id = (
            self._df.groupby(LABEL).apply(lambda x: set(x["ID"].values)).to_dict()
        )
        self._label_to_sex = (
            self._df.groupby(LABEL).apply(lambda x: set(x["SEX"].values)).to_dict()
        )
        self._label_to_path = (
            self._df.groupby(LABEL).apply(lambda x: set(x["FILEPATH"].values)).to_dict()
        )
        self._label_to_name = (
            self._df.groupby(LABEL).apply(lambda x: set(x["NAME"].values)).to_dict()
        )
        self._label_to_category = {
            v: i for i, v in enumerate(sorted(self._df[LABEL].unique()))
        }
        self._label_to_idx = (
            self._df.groupby(LABEL).apply(lambda x: x.index.tolist()).to_dict()
        )
        self._category_to_label = dict(enumerate(sorted(self._df[LABEL].unique())))
        self._category_to_name = {
            self._label_to_category[ID]: NAME
            for ID, NAME in self._label_to_name.items()
        }
        self._category_to_sex = {
            self._label_to_category[ID]: SEX for ID, SEX in self._label_to_sex.items()
        }
        self._category_to_path = {
            self._label_to_category[ID]: PATH
            for ID, PATH in self._label_to_path.items()
        }
        self._category_to_idx = {
            self._label_to_category[ID]: idx for ID, idx in self._label_to_idx.items()
        }
        self._categories = list(self._category_to_label.keys())
        self._seconds_per_name = (
            self._df.groupby("NAME").agg({"SECONDS": "sum"}).to_dict()["SECONDS"]
        )
        self._seconds_per_sex = (
            self._df.groupby("SEX").agg({"SECONDS": "sum"}).to_dict()["SECONDS"]
        )
        self._seconds_per_language = (
            self._df.groupby("LANGUAGE").agg({"SECONDS": "sum"}).to_dict()["SECONDS"]
        )
        self._seconds_per_label = (
            self._df.groupby(LABEL).agg({"SECONDS": "sum"}).to_dict()["SECONDS"]
        )
        self._class_distribution_time = self._df.groupby(LABEL).agg({"SECONDS": "sum"})
        self._class_distribution = self._df[LABEL].value_counts()
        self._weights_per_class = (
            self._class_distribution.max() / self._class_distribution
        ).to_dict()
        self._weights_time_per_class = (
            self._class_distribution_time.max() / self._class_distribution_time
        ).to_dict()["SECONDS"]
        self._tensor_weights_per_class = np.asarray(
            self._df[LABEL].apply(lambda x: self._weights_per_class[x]).to_list()
        )
        self._tensor_weights_time_per_class = np.asarray(
            self._df[LABEL].apply(lambda x: self._weights_time_per_class[x]).to_list()
        )

        return None

    @property
    def df(self) -> pd.DataFrame:
        """ return raw dataframe """
        return self._df

    @property
    def noise_df(self) -> pd.DataFrame:
        """ return df for noise """
        return self._noise_df

    @property
    def len_df(self) -> int:
        """ return len dataframe """
        return len(self._df)

    @property
    def n_classes(self) -> int:
        """ return num speakers"""
        return len(self._df[self.config["label"]].unique())

    @property
    def label_to_id(self) -> dict:
        """return relationship between ids in df and str LABEL which is unique
        id is an audio and each audio could contain 2 speakers name"""
        return self._label_to_id

    @property
    def label_to_sex(self) -> dict:
        """return relationship between LABEL in df and str sex"""
        return self._label_to_sex

    @property
    def label_to_path(self) -> dict:
        """ return paths of each LABEL"""
        return self._label_to_path

    @property
    def label_to_name(self) -> dict:
        """return name to each LABE:"""
        return self._label_to_name

    @property
    def label_to_category(self) -> dict:
        """assign a numerated and ordered number to each df LABEL"""
        return self._label_to_category

    @property
    def label_to_idx(self) -> dict:
        """return relation between LABEL and list of indices in df"""
        return self._label_to_idx

    @property
    def category_to_label(self) -> dict:
        """return category numerated to id"""
        return self._category_to_label

    @property
    def category_to_name(self) -> dict:
        """return name for each numerated ordered category """
        return self._category_to_name

    @property
    def category_to_sex(self) -> dict:
        """return sex for each numerated ordered category """
        return self._category_to_sex

    @property
    def category_to_path(self) -> dict:
        """return list of paths for each category"""
        return self._category_to_path

    @property
    def category_to_idx(self) -> dict:
        """ return idx in df for each category"""
        return self._category_to_idx

    @property
    def classes(self) -> list:
        """ categories in df"""
        return self._categories

    @property
    def seconds_per_name(self) -> dict:
        """ Full speaking time per speaker"""
        return self._seconds_per_name

    @property
    def seconds_per_sex(self) -> dict:
        """Full speaking time per gender"""
        return self._seconds_per_sex

    @property
    def seconds_per_language(self) -> dict:
        """Full speaking time per language"""
        return self._seconds_per_language

    @property
    def seconds_per_label(self):
        """ Full speaking time per required label output"""
        return self._seconds_per_label

    @property
    def class_distribution_time(self) -> dict:
        """ Distribution of each class given seconds spoken"""
        return self._class_distribution_time.to_dict()["SECONDS"]

    @property
    def class_distribution(self) -> dict:
        """ Distribution of each class given number of utterances"""
        return self._class_distribution.to_dict()

    @property
    def weights_per_class(self) -> dict:
        """ Distribution of each class repect the class with more utterances"""
        return self._weights_per_class

    @property
    def weights_time_per_class(self) -> dict:
        """ Distribution of each class given spoken time respect class with more seconds spoken"""
        return self._weights_time_per_class

    @property
    def tensor_weights_per_class(self) -> np.array:
        """ weights_per_class_Tensor"""
        return self._tensor_weights_per_class

    @property
    def tensor_weights_time_per_class(self) -> np.array:
        """ weights_time_per_class Tensor"""
        return self._tensor_weights_time_per_class


class AudioGenerator(SegmentGenerator):  
    def __init__(
        self, config: dict
    ):
        """ Parameters:
                config: Arguments from dataset key in config json file
        """
        self.config = config
        self.ap = Audio(sr=self.config['audio']['sampling_rate'])
        super().__init__(config)

    def _get_voice_tensor(self, value: dict) -> dict:
        
        window_duration = 0.020
        audio = self.ap.read(filepath=value["path"],
                            channel=value["channel"],
                            )
        audio = [utils.cut_or_pad(instance=single_channel, 
                    length=self.config["audio"]["fragment_length"],
                    stochastic=self.config["audio"]["stochastic"]) for single_channel in audio]
        
        if self.segments_:
            channel_segments = [self.ap.webrtc_vad(single_channel,window_duration=window_duration) for single_channel in audio]
            segments = [[1 if segment['is_speech'] else 0 for segment in segments] for segments in channel_segments]
            masks = []  
            for ch, ch_segments in enumerate(segments):
                tmp_mask = np.zeros_like(audio[ch])
                for i, bit in enumerate(ch_segments): 
                    tmp_mask[int(i* self.ap.sr* window_duration): int(i * self.ap.sr * window_duration + self.ap.sr*window_duration)] = bit
                audio[ch] = audio[ch] * tmp_mask 
                masks.append(tmp_mask)
            value["segments"] = to_tensor(segments[0])

        if "noise_path" in value:
            # We support random choice but generate_single return a single sample, but it could be changed
            SNR = np.random.randint(-10,10)
            if np.random.choice([True,False], p=[0.90, 0.1]):
                noise = self.ap.load_noise(value["noise_path"])
                noise = [utils.cut_or_pad(instance=noise[0], length=self.config["audio"]["fragment_length"],
                    stochastic=True)]
                noisy_audio = [self.ap.mix_audios(single_channel, noise[0], SNR) for single_channel in audio]
            else:
                noise = [np.zeros_like(a) for a in audio]
                noisy_audio =  audio
            if self.segments_:
                noise_profile = [single_noise * (1 - masks[idx]) for idx, single_noise in enumerate(noisy_audio)]
                value["noise_profile"] = to_tensor(noise_profile)
            value["noise"] = to_tensor(noise)
            #get_spectrogram(to_tensor(noise),n_fft=512,win_len=400,hop_len=160,power=None).permute((0,3,1,2))
            value["noisy_audio"] = to_tensor(noisy_audio)
            #get_spectrogram(to_tensor(noisy_audio),n_fft=512,win_len=400,hop_len=160,power=None).permute((0,3,1,2))
        
        value["audio"] = to_tensor(audio)
        #get_spectrogram(to_tensor(audio),n_fft=512,win_len=400,hop_len=160,power=None).permute((0,3,1,2))
        #value["category"] =  torch.as_tensor(value["category"])
        return value