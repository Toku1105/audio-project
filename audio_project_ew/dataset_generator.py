import numpy as np
import logging
import torch
logger = logging.getLogger('develop')

class Generator(torch.utils.data.Dataset):
    def __init__(self, 
        generator_method: str = 'simple',
        phase: str = 'TRAIN',
        label: str = '',
        classes: list = [],
        datasets: list = [],
        **kwargs):
        """ Generator for Pytorch
        Parameters
            batch_size(int) : size of batch
            balance(bool): if return a normal distribution of dataset
            sampling(int): oversample dataset
            gen_method(str): simple or triplets
            phase(str): TRAIN stage or TEST
            classes(list): list of classes to use or filter
            superclass(str): Kind of data to use from the dataset ex. LANGUAGE, SEX, NAME.
            datasets(list): list containing names of datasets to use

        """
        self._gen_type = generator_method
        self.phase = phase
        self.label = label
        self.classes_ = classes
        self._df = None
        self.noise_ = kwargs['audio']['noise']
        self.segments_ = kwargs['audio']['segments']
        self._noise_df = None
        np.random.seed(kwargs["seed"])

        self._datasets = datasets

        if isinstance(self._datasets, str):
            self._datasets = [self._datasets]

        if not len(self._datasets):
            raise Exception("No datasets selected")
        #self.datasets = list(map(str.upper, datasets))

    def _generate_triplets(self, idx: int) -> dict:
        """ generate random triplets"""
        raise NotImplementedError("_generate_triplets method not implemented")

    def _generate_simple(self, idx: int) -> dict:
        """Generate random call"""
        raise NotImplementedError("_generate_simple method not implemented")

    def _get_voice_tensor(self, properties: dict) -> dict:
        """ get torch tensors  from audio"""
        raise NotImplementedError("_get_voice_tensor_label method not implemented")

    def _compute_output(self, element: list):
        
        keys_to_drop = []
        for key, el in element.items():
            if not isinstance(el,torch.Tensor):
                keys_to_drop.append(key)
        for key in keys_to_drop:
            del element[key]

        return element#(X,y)

    def _data_generator(self, idx):
        # TODO: FIX triplets

        if self._gen_type == "triplets":
            elements = self._generate_triplets(idx)
            result = []
            for triplet in elements:
                result.append(
                    {
                        key: self._get_voice_tensor(element)
                        for key, element in triplet.items()
                    }
                )

        elif self._gen_type == "simple":
            element = self._generate_single(idx)
            tmp_dict = self._get_voice_tensor(element)
            logger.info(tmp_dict)
        else:
            raise Exception(f"Generator Type {self._gen_type} not implemented")

        #X, y 
        output = self._compute_output(tmp_dict)

        return output #X, y

    def __getitem__(self, idx: int):
        """ get batch at posiition index"""
        logger.info(f"Get Index: {idx}")
        #X,y
        output = self._data_generator(idx)
        return output #X, y

    def __len__(self):
        """ Size of dataset"""
        return len(self._df)
