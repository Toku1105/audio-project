import os
import math
import struct
import torch
import librosa
import webrtcvad
import numpy as np
import soundfile as sf
from .utils import cut_or_pad, normalize_audio
from .model import VoiceActivityDetector
from .torch_utils import to_parallel, to_device, get_default_device
from .inference import InferVAD, InferDenoiser


import logging
logger = logging.getLogger('develop')


class Audio:
    
    def __init__(self, 
                 sr:int=16000,filepath:str=None):
        self.sr = sr
        self.filepath = filepath
        self.audio = self.read(filepath) if filepath else None 
        self.mode_inference = 'cpu'
        self.deep_denoiser_model = None
        
    
    def read(self,
             filepath:str,
             mono:bool=False,
             channel:int=None,
             sr:int=None,
             )->list:
        ''' Read audio.
            Args:
                path: File path.
                mono: if False(default) read all channels
                channel: if mono=True return channel selected, otherwise return all channels 
                sr: sampling rate to read, default(self sr instance)
            Returns:
                audio(list(arrays)): return list of arrays each channel
        '''
        sr = sr if sr else self.sr
        self.filepath = filepath
        try:
            audio, sr = librosa.load(self.filepath,
                                     sr=sr,
                                     mono=mono)

            if channel is not None and isinstance(audio[channel], np.ndarray):
                try:
                    audio = np.expand_dims(audio[channel], axis=0)
                except:
                    logger.warning(f"Channel {channel} does not exist: using default 0")
                    audio = np.expand_dims(audio[0], axis=0)
            else:
                audio = np.expand_dims(audio, axis=0)

        except Exception as ex:
            logger.exception(f"Exception opening file with librosa. File {self.filepath}, exception: {ex}")
            return []

        tmp_audio = []
        for channel in audio:
            a = normalize_audio(channel)
            tmp_audio.append(a)
        self.audio = tmp_audio
        return self.audio

    def deep_denoiser(self,
                     audio:np.ndarray,
                     model=None,
                     vad_path:str=None,
                     denoiser_path:str=None,
                     mode:str='cpu'):
        # if not vad_path:
        #     name = 'best_acc'
        #     vad_path = os.path.join('../output/models/VAD', f"{name}.pth")
        # if not denoiser_path:
        #     name = 'best_loss'
        #     denoiser_path = os.path.join('../output/models/DENOISER', f"{name}.pth")
        # if not self.mode_inference: self.mode_inference = mode
        # if not self.deep_denoiser_model:
        #     vad_model = self.deep_vad_model.vad_model if self.deep_vad_model else None 
        #     self.deep_denoiser_model = self.deep_vad_model = InferDenoiser(vad_path, 
        #                                                     denoiser_path,
        #                                                     mode=self.mode_inference,
        #                                                     vad_model=vad_model)
        output = model.runDenoiser(audio)
        return output

    def deep_vad(self,
                 audio:np.ndarray,
                 model = None, 
                 model_path:str=None,
                 sr:int=16000,
                 window_duration:float=0.020,
                 threshold:float=0.5,
                 mode='cpu'
                 )->list:

        ''' Our DeepVAD
            Args:
                audio(np.array): audio only 1 channel
                windows_duration(float): duration of windows ms 
                threshold: from 0.0 to 1.0 probability selection
                mode: 'cpu' or 'gpu'
                sr: sampling rate to read, default(self sr instance)
            Returns:
                segments(list): return list of segments with boolean True if is speech otherwise False
        '''
        # if not model_path:
        #     name = 'best_acc'
        #     model_path = os.path.join('../output/models/VAD', f"{name}.pth")
        
        # if not self.mode_inference: self.mode_inference = mode
        # if not self.deep_denoiser_model: self.deep_denoiser_model = InferVAD(model_path, mode=self.mode_inference)
        
        segments,_,spec = model.audio_from_VAD(audio)
        return segments

    
    def kaldi_vad(self,
                  audio:np.ndarray,
                  sr:int=None,)->list:
        ''' VAD Kaldi
            Args:
                audio(np.array): audio only 1 channel
                sr: sampling rate to read, default(self sr instance)
            Returns:
                segments(list): return list of segments with boolean True if is speech otherwise False
        '''
        try:
            import bob.kaldi
        except ModuleNotFoundError as ie:
            print(ie('bob.kaldi is not installed unable to use kaldivad use webrtc_vad instead'))
            return
        
        sr = sr if sr else self.sr
        VAD_labels = bob.kaldi.compute_vad(audio,sr)
        segments = []      
        for section in Audio._pos_section(VAD_labels):
            segments.append({'start':int(section[0]*self.sr*0.010), 
                        'stop':int(section[1]*self.sr*0.010),
                        'is_speech': True})
        return segments
    
    def webrtc_vad(self,
            audio:np.ndarray,
            aggressiveness:int=3,
            window_duration:float=0.020,
            bytes_per_sample:int=2,
            sr:int=None,)-> list:
        ''' VAD webrtc
            Args:
                audio(np.array): audio only 1 channel
                aggressiveness(int): 0-3
                windows_duration(float): duration of windows ms 
                bytes_per_sample(int): 2 default int16, at this moment different value is not supported
                sr: sampling rate to read, default(self sr instance)
            Returns:
                segments(list): return list of segments with boolean True if is speech otherwise False
        '''
        sr = sr if sr else self.sr
        audio = (audio * 32767).astype(np.int16)
        vad = webrtcvad.Vad()
        vad.set_mode(aggressiveness)
        samples_per_window = int(window_duration * sr)
        segments = []
        audio_bytes = struct.pack("%dh" % len(audio), *audio)  # this only works for int16
        for start in np.arange(0, len(audio), samples_per_window):
            stop = min(start + samples_per_window, len(audio))
            samp = audio_bytes[start * bytes_per_sample : stop * bytes_per_sample]
            # complete bytearray if size is not divisible by 16bits*2*10
            size = 16 * bytes_per_sample * 10
            to_add = (size - len(samp) % (size)) % (size)
            correction = b"\x00" * to_add
            is_speech = vad.is_speech((samp + correction), sample_rate=sr)
            segments.append(dict(start=start, stop=stop, is_speech=is_speech))

        return segments
    
    def cutter(self,
               audio:np.ndarray,
               segments:list,
               sr:int=None,
               gap:float=0.0,)->np.ndarray:
        ''' audio cutter, cut and join segments or audio given a dictionary conatining a bool isspeech
            Args:
                audio(np.ndarray): audio only 1 channel
                segments(list(dict)): list of dictionaries including flag isspeech, start and stop of segment
                sr(int): sampling rate
                gap(float): seconds to add its calculated as sr*gap
            Returns:
                audio(np.ndarray): segmented array
        '''
               
        tmp_list = []
        sr = sr if sr else self.sr
        
        for segment in segments:
            if segment["is_speech"]:
                tmp_list.append(audio[segment["start"] : segment["stop"]])
                tmp_list.append([0.0]*int(sr*gap))
        audio = np.concatenate(tmp_list) if tmp_list else []
        
        return audio

    def mix_audios(self,
                   audio:np.ndarray,
                   noise:np.ndarray, 
                   SNR:float,
                   stochastic:bool=True,
                   mode:str='wrap',)->np.ndarray:
        ''' Mix two audios given SNR 
            Args:
                audio(np.ndarray): audio reference.
                noise(np.ndarray): noise to add
                SNR(float): SNR value to mix
                stochastic(bool): if audio and noise have different sizes, 
                                  they will be mixed by cutting or padding noise signal,
                                  to get same size of audio signal otherwise padding will
                                  be at then end of the array
            Returns:
                audio(list): list of np.ndarrays(audios)
        '''
        noise = cut_or_pad(noise, len(audio), stochastic, mode)
        # snr = math.exp(SNR/10)
        # scale = snr * np.linalg.norm(noise)/np.linalg.norm(audio)
        # noise = (scale*audio+noise)/2
        # return noise
        
        RMS_s = math.sqrt(np.mean(audio**2))
        RMS_n = math.sqrt(RMS_s**2/(pow(10,SNR/10)))
        RMS_n_current = math.sqrt(np.mean(noise**2))
        noise = noise*(RMS_n/RMS_n_current)
        audio = audio + noise
        audio = normalize_audio(audio)
        return audio
    
    def save_audio(self, 
                   data:np.ndarray=None,
                   path:str=None,
                   sr:int=None)->str:
        ''' Save file audio given path and sampling rate
            Args:
                data(np.ndarray): audio 
                path(str): path to save
                sr(int): sampling rate
            Returns:
                tuple with path and name of audio
        '''
        data = data if data is not None else self.audio[0]
        sr = sr if sr else self.sr
        path = path if path else self.filepath
        if path == self.filepath:
            logger.info(f'Updating Audio to same path: {self.filepath}')
        if not path or os.path.splitext(path)[-1] != '.wav':
            raise ValueError('Provide path to save audio')
        
        sf.write(path, data, samplerate=sr)
        logger.info(f'Audio file saved {path}')
        
        return os.path.split(path)

    def silence_noise(self,
               audio:np.ndarray,
               segments:list,
               gap:float=0.0,)->np.ndarray:
        ''' audio cutter, cut and join segments or audio given a dictionary conatining a bool isspeech
            Args:
                audio(np.ndarray): audio only 1 channel
                segments(list(dict)): list of dictionaries including flag isspeech, start and stop of segment
                sr(int): sampling rate
                gap(float): seconds to add its calculated as sr*gap
            Returns:
                audio(np.ndarray): segmented array
        '''
        tmp_list = []
        
        for segment in segments:
            if segment["is_speech"]:
                tmp_list.append(audio[segment["start"] : segment["stop"]])
            else:
                tmp_list.append([0.0]*int(segment["stop"] - segment["start"]))
        audio = np.concatenate(tmp_list) if tmp_list else []
        
        return audio

    def extract_segment(self, 
                        data:np.ndarray=None,
                        start:float=None,
                        end:float=None,
                        sr:int=None)->np.ndarray:
        ''' Extract segment of audio given start(secs) and end(secs)
            Args:
                data(np.ndarray): audio 
                sr(int): sampling rate
            Returns:
                np.ndarray with segment extracted
        '''
        data = data if data is not None else self.audio[0]
        sr = sr if sr else self.sr
        start = int(sr*start) 
        end = int(sr*end)
        if end >len(data):
            logger.warning(f'Asked end: {end} however audio lenght is {len(data)} returning maximum')
            end = len(data)
        if start>=len(data):
            raise ValueError(f'Impossible to get segment from {start} to {end} in audio with len: {len(data)}')
        
        return data[start:end]

    def split_audio(
        self,
        audio:np.ndarray,
        ID:str,
        channel: int,
        filepath: str=None,
        seconds_per_split: int=180,
        sampling_rate: int=None,
        store_split: bool=True,
        )->list:
        """
        Return a list of details of new segmented audios saved in same folder new name
            Arguments:
                audio(np.ndarray): Input to split default self
                filepath(str): Path for audio with name default self.filepath
                sampling_rate(int): sampling rate to save audios default self.sr
                seconds_per_split(int): seconds in each audio
                store_split(bool): store splits in same path or not
                channel(int): channel of the audio for self.audio
        """
        sampling_rate = sampling_rate if sampling_rate else self.sr
        filepath = filepath if filepath else self.filepath
        audio = audio if audio is not None else self.audio[channel]

        output = []
        samples = sampling_rate * seconds_per_split

        for start in range(0, len(audio), samples):
            segment_details = {}
            stop = min(start + samples, len(audio))
            segment = audio[start:stop]

            path = (
                os.path.splitext(filepath)[0]
                + f".split-{start}-{stop}-{channel}.wav"
            )
            if store_split:
                sf.write(path, segment, samplerate=sampling_rate)
            else:
                logger.warning(
                    f'[WARNING] Splitting audio {details["ID"]} but you set store_split as False \
                        if you want to save audio set it as True'
                )
            segment_details["FILEPATH"] = path
            segment_details["SECONDS"] = len(segment) * 1.0 / sampling_rate
            segment_details['LENGTH'] = len(segment)
            segment_details["CHANNEL"] = channel
            segment_details["ID"] = ID
            output += [segment_details]
        return output


    @staticmethod
    def load_noise(paths,sr:int=16000):
        ''' Load audios from a list of paths or single str path 
            Args:
                paths(str or list): of strings with audio to load
            Returns:
                audio(list): list of np.ndarrays(audios)
        '''
        paths = [paths] if isinstance(paths, str) else paths
        output = []
        for path in paths:
            if os.path.exists(path):
                noise,sr = librosa.load(path, sr=sr, mono=True)
                noise = normalize_audio(noise)
                output.append(noise)
        return output      
    
    @staticmethod
    def _pos_section(arr, th=0.5):
        """
            Return: list of session [left, right]
        """
        cur_i = 0
        while(True):
            # skip lower item
            while(cur_i < len(arr) and arr[cur_i] < th):
                cur_i += 1
            if cur_i >= len(arr):
                break

            # iter higer item
            left_i = cur_i
            while (cur_i < len(arr) and arr[cur_i] >= th):
                cur_i += 1

            yield left_i, cur_i
            cur_i += 1
             
